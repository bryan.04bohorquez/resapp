<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'DashboardController@index')->name('dashboard')->middleware('usertype');

Route::get('/users', 'UsersController@index')->name('users')->middleware('usertype');
Route::get('/users/data', 'UsersController@data')->name('users_data')->middleware('auth');
Route::get('/users/export', 'UsersController@export')->name('export');
Route::resource('users', 'UsersController', ['except' => 'show', 'create', 'edit'])->middleware('usertype');

Route::get('/customers', 'CustomersController@index')->name('customers')->middleware('usertype');
Route::get('/customers/data', 'CustomersController@data')->name('customers_data');
Route::get('/customers/export', 'CustomersController@export')->name('export')->middleware('usertype');
Route::post('/customers/namesearch', 'CustomersController@nameSearch')->name('nameSearch');
Route::resource('customers', 'CustomersController', ['except' => 'show', 'create', 'edit'])->middleware('usertype');

Route::get('/products', 'ProductsController@index')->name('products')->middleware('usertype');
Route::get('/products/data', 'ProductsController@data')->name('products_data');
Route::resource('products', 'ProductsController', ['except' => 'show', 'create', 'edit'])->middleware('usertype');
Route::get('/products/export', 'ProductsController@export')->name('export')->middleware('usertype');
Route::post('/products/namesearch', 'ProductsController@nameSearch')->name('nameSearch');
Route::post('/products/list', 'ProductsController@productList')->name('productList');
Route::post('/products/ingredients', 'ProductsController@ingredients')->name('ingredients')->middleware('usertype');

Route::get('/dishes', 'DishesController@index')->name('dishes')->middleware('usertype');
Route::get('/dishes/data', 'DishesController@data')->name('dishes_data');
Route::post('/dishes/list', 'DishesController@dishesList')->name('dishesList');
Route::resource('dishes', 'DishesController', ['except' => 'show', 'create', 'edit'])->middleware('usertype');

Route::get('/categories', 'CategoriesController@index')->name('categories')->middleware('usertype');
Route::get('/categories/data', 'CategoriesController@data')->name('categories_data')->middleware('usertype');
Route::resource('categories', 'CategoriesController', ['except' => 'show', 'create', 'edit'])->middleware('usertype');
Route::get('/categories/export', 'CategoriesController@export')->name('export')->middleware('usertype');

Route::get('/providers', 'ProvidersController@index')->name('providers')->middleware('usertype');
Route::get('/providers/data', 'ProvidersController@data')->name('providers_data')->middleware('usertype');
Route::resource('providers', 'ProvidersController', ['except' => 'show', 'create', 'edit'])->middleware('usertype');
Route::get('/providers/export', 'ProvidersController@export')->name('export')->middleware('usertype');

Route::get('/invoices', 'InvoicesController@index')->name('invoices')->middleware('usertype');
Route::get('/invoices/data/', 'InvoicesController@data')->name('providers_data');
Route::post('/invoices/report', 'InvoicesController@report')->name('report')->middleware('usertype');
Route::get('/invoices/data/{id}', 'InvoicesController@data')->name('providers_data');
Route::resource('invoices', 'InvoicesController', ['except' => 'show', 'create', 'edit'])->middleware('usertype');
Route::get('/invoices/export', 'InvoicesController@export')->name('export')->middleware('usertype');
Route::get('/invoice/pdf/{invoice}', 'InvoicesController@invoice')->name('pdf');

Route::get('/sell', 'SellController@index')->name('sell');

