const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.scripts([
             'resources/js/jquery.min.js',
             'resources/js/toastr.min.js',
             'resources/js/popper.min.js',
             'resources/js/bootstrap.min.js',
             'resources/js/axios.js',
             'resources/js/vue.js',
             'resources/js/datatables.min.js',
             'resources/js/bootstrap-datepicker.min.js',
             'resources/js/bootstrap-datepicker.es.min.js',
             'resources/js/dataTables.bootstrap4.min.js','resources/js/sweetalert2.js',
             'resources/js/app.js',],
             'public/js/app.js')
   .sass('resources/sass/app.scss', 'public/css');
