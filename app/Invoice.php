<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    protected $fillable = [
        'customer_id',
        'paid_value',
        'invoice_status_id',
    ];

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function products()
    {
        return $this->hasMany(InvoiceProducts::class);
    }

    public function status()
    {
        return $this->belongsTo(InvoiceStatus::class, 'invoice_status_id');
    }
}
