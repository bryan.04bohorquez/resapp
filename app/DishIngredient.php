<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DishIngredient extends Model
{
    protected $fillable = [
        'dish_id',
        'product_id',
        'quantity',
        'total',
    ];

    public function dish()
    {
        return $this->belongsTo(Dish::class);
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

}
