<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class UserType extends Model
{

    protected $table = 'user_types';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 
    ];

    public function users()
    {
        return $this->hasMany(User::class);
    }
}
