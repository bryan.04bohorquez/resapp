<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvoiceProducts extends Model
{
    protected $fillable = [
        'invoice_id',
        'dish_id',
        'product_id',
        'quantity',
        'price',
    ];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function dish()
    {
        return $this->belongsTo(Dish::class);
    }

    public function invoice()
    {
        return $this->belongsTo(Invoice::class);
    }
}
