<?php

namespace App\Http\Controllers;

use App\User;
use App\Person;
use App\UserType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Datatables;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class UsersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $users = User::all();
        $user_types = UserType::all();
        return view('users', compact('users', 'user_types'));
    }

    public function data()
    {
        $users = User::with('person', 'userType')->get();
        return $users;
    }

    public function store(Request $request)
    {

        $this->validate($request, [

            //Person Data
            'first_name' => 'required',
            'last_name' => 'required',
            'identification' => 'required',
            'phone' => 'required',

            //User Data
            'username' => 'required',
            'email' => 'required',
            'password' => 'required',

        ]);

        $person = Person::create([
            'first_name' => $request['first_name'],
            'last_name' => $request['last_name'],
            'identification' => $request['identification'],
            'phone' => $request['phone'],
        ]);

        User::create([
            'username' => $request['username'],
            'person_id' => $person->id,
            'user_type_id' => $request['user_type'],
            'email' => $request['email'],
            'password' => Hash::make($request['password']),
        ]);

        return;
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [

            //Person Data
            'first_name' => 'required',
            'last_name' => 'required',
            'identification' => 'required',
            'phone' => 'required',

            //User Data
            'username' => 'required',
            'email' => 'required',
            'password' => 'required',

        ]);

        $user = User::find($id);

        $user->update([
            'username' => $request['username'],
            'user_type_id' => $request['user_type'],
            'email' => $request['email'],
            'password' => Hash::make($request['password']),
        ]);

        Person::find($user->person_id)->update([
            'first_name' => $request['first_name'],
            'last_name' => $request['last_name'],
            'identification' => $request['identification'],
            'phone' => $request['phone'],
        ]);

        return;
    }

    public function destroy($id)
    {
        $user = User::find($id);
        $person = Person::find($user->person_id)->delete();
        $user->delete();
    }

    public function export()
    {
        $date = date('m_d_y');
        $url = public_path().'/tmp/users_'. $date .'.xlsx';

        $users = $this->data();

        $styleArray = [
            'borders' => [
                'outline' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '000000'],
                ],
            ],
        ];

        $headerStyle = [
            'font' => [
                'bold' => true,
            ],
            'borders' => [
                'outline' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => \PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE],
                ],
            ],
            'fill' => [
                'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                'startColor' => [
                    'argb' => \PhpOffice\PhpSpreadsheet\Style\Color::COLOR_BLACK,
                ],
            ]
        ];

        $row = 2;

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $columns = [
            'A' => 'ID',
            'B' => 'Nombre',
            'C' => 'Identificación',
            'D' => 'Tipo',
            'E' => 'Telefono',
            'F' => 'Correo Electronico',
        ];

        foreach ($columns as $column => $name)
        {
            $sheet->getColumnDimension($column)->setAutoSize(true);
            $sheet->setCellValue($column.'1', $name);
            $sheet->getStyle($column.'1')->applyFromArray($headerStyle);

            $sheet->getStyle($column.'1')->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
        }

        foreach ($users as $user){

            $sheet->setCellValue('A'.$row, $user->id);
            $sheet->getStyle('A'.$row)->applyFromArray($styleArray);

            $sheet->setCellValue('B'.$row, $user->person->first_name . " " . $user->person->last_name);
            $sheet->getStyle('B'.$row)->applyFromArray($styleArray);

            $sheet->setCellValue('C'.$row, $user->person->identification);
            $sheet->getStyle('C'.$row)->applyFromArray($styleArray);

            $sheet->setCellValue('D'.$row, $user->userType->name);
            $sheet->getStyle('D'.$row)->applyFromArray($styleArray);

            $sheet->setCellValue('E'.$row, $user->person->phone);
            $sheet->getStyle('E'.$row)->applyFromArray($styleArray);

            $sheet->setCellValue('F'.$row, $user->email);
            $sheet->getStyle('F'.$row)->applyFromArray($styleArray);

            $row += 1;
        }

        $writer = new Xlsx($spreadsheet);
        $writer->save($url);

        return '/tmp/users_'. $date .'.xlsx';
    }
}
