<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Person;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class CustomersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $customers = Customer::all();
        return view('customers', compact('customers'));
    }

    public function data()
    {
        $customers = Customer::with('person')->get();
        return $customers;
    }

    public function nameSearch(Request $request) {

        if ($request['name'])
        {
            $customer = DB::table('customers')
                ->select ('customers.id as customer_id', 'persons.first_name', 'persons.last_name', 'persons.identification', 'persons.phone')
                ->Join ('persons', 'persons.id', '=', 'customers.person_id')
                ->where('persons.identification', 'LIKE', $request['name'].'%')
                ->get();

            return $customer;
        }
        else
        {
            return;
        }
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
            'identification' => 'required',
            'phone' => 'required',
        ]);

        $person = Person::create([
            'first_name' => $request['first_name'],
            'last_name' => $request['last_name'],
            'identification' => $request['identification'],
            'phone' => $request['phone'],
        ]);

        $customer = Customer::create([
            'person_id' => $person->id,
        ]);

        return $customer->id;
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
            'identification' => 'required',
            'phone' => 'required',
        ]);

        $customer = Customer::find($id);

        Person::find($customer->person_id)->update([
            'first_name' => $request['first_name'],
            'last_name' => $request['last_name'],
            'identification' => $request['identification'],
            'phone' => $request['phone'],
        ]);

        return;
    }

    public function destroy($id)
    {
        $customer = Customer::find($id);
        $person = Person::find($customer->person_id)->delete();
        $customer->delete();
    }

    public function export()
    {
        $date = date('m_d_y');
        $url = public_path().'/tmp/customers_'. $date .'.xlsx';

        $customers = $this->data();

        $styleArray = [
            'borders' => [
                'outline' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '000000'],
                ],
            ],
        ];

        $headerStyle = [
            'font' => [
                'bold' => true,
            ],
            'borders' => [
                'outline' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => \PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE],
                ],
            ],
            'fill' => [
                'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                'startColor' => [
                    'argb' => \PhpOffice\PhpSpreadsheet\Style\Color::COLOR_BLACK,
                ],
            ]
        ];

        $row = 2;

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $columns = [
            'A' => 'ID',
            'B' => 'Nombre',
            'C' => 'Identificación',
            'D' => 'Telefono',
        ];

        foreach ($columns as $column => $name)
        {
            $sheet->getColumnDimension($column)->setAutoSize(true);
            $sheet->setCellValue($column.'1', $name);
            $sheet->getStyle($column.'1')->applyFromArray($headerStyle);

            $sheet->getStyle($column.'1')->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
        }

        foreach ($customers as $customer){

            $sheet->setCellValue('A'.$row, $customer->id);
            $sheet->getStyle('A'.$row)->applyFromArray($styleArray);

            $sheet->setCellValue('B'.$row, $customer->person->first_name . " " . $customer->person->last_name);
            $sheet->getStyle('B'.$row)->applyFromArray($styleArray);

            $sheet->setCellValue('C'.$row, $customer->person->identification);
            $sheet->getStyle('C'.$row)->applyFromArray($styleArray);

            $sheet->setCellValue('D'.$row, $customer->person->phone);
            $sheet->getStyle('D'.$row)->applyFromArray($styleArray);

            $row += 1;
        }

        $writer = new Xlsx($spreadsheet);
        $writer->save($url);

        return '/tmp/customers_'. $date .'.xlsx';
    }
}
