<?php

namespace App\Http\Controllers;

use App\Person;
use App\Provider;
use Illuminate\Http\Request;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class ProvidersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $providers = Provider::all();
        return view('providers', compact('providers'));
    }

    public function data()
    {
        $providers = Provider::with('representant', 'products')->get();
        return $providers;
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'address' => 'required',
            'phone' => 'required',
            'payment_method' => 'required',
            'bank_account' => 'required',
            'delivery_frequency' => 'required',
        ]);

        $person = [];

        if ($request['p_first_name'])
        {
            $person = Person::create([
                'first_name' => $request['p_first_name'],
                'last_name' => $request['p_last_name'],
                'identification' => $request['p_identification'],
                'phone' => $request['p_phone'],
            ]);
        }

        Provider::create([
            'type' => $request['type'],
            'name' => $request['name'],
            'business_name' => $request['business_name'],
            'address' => $request['address'],
            'phone' => $request['phone'],
            'nit' => $request['nit'],
            'payment_method' => $request['payment_method'],
            'bank_account' => $request['bank_account'],
            'iva' => $request['iva'],
            'delivery_frequency' => $request['delivery_frequency'],
            'representant_id' => $person->id ?? null,
        ]);

        return;
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'address' => 'required',
            'phone' => 'required',
            'payment_method' => 'required',
            'bank_account' => 'required',
            'delivery_frequency' => 'required',
        ]);

        $provider = Provider::find($id);

        $provider->update([
            'name' => $request['name'],
            'business_name' => $request['business_name'],
            'address' => $request['address'],
            'phone' => $request['phone'],
            'nit' => $request['nit'],
            'payment_method' => $request['payment_method'],
            'bank_account' => $request['bank_account'],
            'iva' => $request['iva'],
            'delivery_frequency' => $request['delivery_frequency'],
        ]);

        $person = [];

        if ($request['p_first_name'])
        {
            if (!is_null($provider->representant_id))
            {
                $person = Person::find($id)->update([
                    'first_name' => $request['p_first_name'],
                    'last_name' => $request['p_last_name'],
                    'identification' => $request['p_identification'],
                    'phone' => $request['p_phone'],
                ]);
            }
            else
            {
                $person = Person::create([
                    'first_name' => $request['p_first_name'],
                    'last_name' => $request['p_last_name'],
                    'identification' => $request['p_identification'],
                    'phone' => $request['p_phone'],
                ]);

                Provider::find($id)->update([
                    'representant_id' => $person->id,
                ]);
            }
        }

        return;
    }

    public function destroy($id)
    {
        $product = Provider::find($id);
        $product->delete();
    }

    public function export()
    {
        $date = date('m_d_y');
        $url = public_path().'/tmp/providers_'. $date .'.xlsx';

        $providers = $this->data();

        $styleArray = [
            'borders' => [
                'outline' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '000000'],
                ],
            ],
        ];

        $headerStyle = [
            'font' => [
                'bold' => true,
            ],
            'borders' => [
                'outline' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => \PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE],
                ],
            ],
            'fill' => [
                'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                'startColor' => [
                    'argb' => \PhpOffice\PhpSpreadsheet\Style\Color::COLOR_BLACK,
                ],
            ]
        ];

        $row = 2;

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $columns = [
            'A' => 'ID',
            'B' => 'Nombre',
            'C' => 'Razon Social',
            'D' => 'Dirección',
            'E' => 'Telefono',
            'F' => 'Nit',
            'G' => 'Metodo de Pago',
            'H' => 'Cuenta de Banco',
            'I' => 'IVA',
            'J' => 'Frecuencia de Pago',
            'K' => 'Representante',
        ];

        foreach ($columns as $column => $name)
        {
            $sheet->getColumnDimension($column)->setAutoSize(true);
            $sheet->setCellValue($column.'1', $name);
            $sheet->getStyle($column.'1')->applyFromArray($headerStyle);

            $sheet->getStyle($column.'1')->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
        }

        foreach ($providers as $provider){

            $sheet->setCellValue('A'.$row, $provider->id);
            $sheet->getStyle('A'.$row)->applyFromArray($styleArray);

            $sheet->setCellValue('B'.$row, $provider->name);
            $sheet->getStyle('B'.$row)->applyFromArray($styleArray);

            $sheet->setCellValue('C'.$row, $provider->business_name ?? 'No aplica');
            $sheet->getStyle('C'.$row)->applyFromArray($styleArray);

            $sheet->setCellValue('D'.$row, $provider->address);
            $sheet->getStyle('D'.$row)->applyFromArray($styleArray);

            $sheet->setCellValue('E'.$row, $provider->phone);
            $sheet->getStyle('E'.$row)->applyFromArray($styleArray);

            $sheet->setCellValue('F'.$row, $provider->nit ?? 'No aplica');
            $sheet->getStyle('F'.$row)->applyFromArray($styleArray);

            $sheet->setCellValue('G'.$row, $provider->payment_method);
            $sheet->getStyle('G'.$row)->applyFromArray($styleArray);

            $sheet->setCellValue('H'.$row, $provider->bank_account);
            $sheet->getStyle('H'.$row)->applyFromArray($styleArray);

            $sheet->setCellValue('I'.$row, $provider->iva);
            $sheet->getStyle('I'.$row)->applyFromArray($styleArray);

            $sheet->setCellValue('J'.$row, $provider->delivery_frequency);
            $sheet->getStyle('J'.$row)->applyFromArray($styleArray);

            $sheet->setCellValue('K'.$row, ($provider['representant']) ? $provider['representant']['first_name'] . " " . $provider['representant']['last_name'] : 'No Aplica');
            $sheet->getStyle('K'.$row)->applyFromArray($styleArray);

            $row += 1;
        }

        $writer = new Xlsx($spreadsheet);
        $writer->save($url);

        return '/tmp/providers_'. $date .'.xlsx';
    }
}
