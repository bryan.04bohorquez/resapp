<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Invoice;
use App\InvoiceProducts;
use App\InvoiceStatus;
use App\Product;
use Carbon\Carbon;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class InvoicesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $invoices = Invoice::all();
        $status = InvoiceStatus::all();
        return view('invoices', compact('invoices', 'status'));
    }

    public function data($id = false)
    {
        if (!$id) {
            $invoices = Invoice::with('products.product', 'products.dish', 'status', 'customer.person')->get();
        } else {
            $invoices = Invoice::Where([['invoice_status_id', '=', $id]])->with('products.product', 'products.dish', 'status', 'customer.person')->get();
        }
        return $invoices;
    }

    public function report(Request $request)
    {
        $ini = Carbon::createFromFormat('d/m/Y', $request->start_date);
        $end = Carbon::createFromFormat('d/m/Y', $request->end_date);

        $invoices = Invoice::with('products.product', 'products.dish', 'status', 'customer.person')->whereBetween('updated_at', [$ini, $end])->get();

        $date = date('m_d_y');
        $url = public_path().'/tmp/sell_report_'. $date .'.xlsx';

        $products = $this->data();

        $styleArray = [
            'borders' => [
                'outline' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '000000'],
                ],
            ],
        ];

        $headerStyle = [
            'font' => [
                'bold' => true,
            ],
            'borders' => [
                'outline' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => \PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE],
                ],
            ],
            'fill' => [
                'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                'startColor' => [
                    'argb' => \PhpOffice\PhpSpreadsheet\Style\Color::COLOR_BLACK,
                ],
            ]
        ];

        $row = 2;
        $sells_total = 0;

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $columns = [
            'A' => 'ID',
            'B' => 'Cliente',
            'C' => 'Estado',
            'D' => 'Valor',
        ];

        foreach ($columns as $column => $name)
        {
            $sheet->getColumnDimension($column)->setAutoSize(true);
            $sheet->setCellValue($column.'1', $name);
            $sheet->getStyle($column.'1')->applyFromArray($headerStyle);

            $sheet->getStyle($column.'1')->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
        }

        foreach ($invoices as $invoice){

            $sheet->setCellValue('A'.$row, $invoice->id);
            $sheet->getStyle('A'.$row)->applyFromArray($styleArray);

            $sheet->setCellValue('B'.$row, $invoice->customer->person->first_name . " " . $invoice->customer->person->last_name);
            $sheet->getStyle('B'.$row)->applyFromArray($styleArray);

            $sheet->setCellValue('C'.$row, $invoice->status->name);
            $sheet->getStyle('C'.$row)->applyFromArray($styleArray);

            $sheet->setCellValue('D'.$row, $invoice->paid_value);
            $sheet->getStyle('D'.$row)->applyFromArray($styleArray);

            $row += 1;
            $sells_total += $invoice->paid_value;
        }

        $sheet->setCellValue('C'.$row,"TOTAL");
        $sheet->getStyle('C'.$row)->applyFromArray($styleArray);

        $sheet->setCellValue('D'.$row, $sells_total);
        $sheet->getStyle('D'.$row)->applyFromArray($styleArray);

        $writer = new Xlsx($spreadsheet);
        $writer->save($url);

        return '/tmp/sell_report_'. $date .'.xlsx';
    }

    public function store(Request $request)
    {
        $products = $request->products;

        if (!$request['customer_id']) {
            $customer = new CustomersController();
            $customer = $customer->store($request);
        } else {
            $customer = $request['customer_id'];
        }

        $invoice = Invoice::create([
            'customer_id' => $customer,
            'paid_value' => $request['paid_value'],
            'invoice_status_id' => $request['status']
        ]);

        foreach ($products as $product) {
            if (isset($product['brand'])) {
                InvoiceProducts::create([
                    'invoice_id' => $invoice['id'],
                    'product_id' => $product['id'],
                    'quantity' => $product['quantity'],
                    'price' => $product['total'],
                ]);

                Product::find($product['id'])->update([
                    'stock' => $product['stock'] - $product['quantity'],
                ]);
            } else {
                $invoice = InvoiceProducts::create([
                    'invoice_id' => $invoice['id'],
                    'dish_id' => $product['id'],
                    'quantity' => $product['quantity'],
                    'price' => $product['total'],
                ]);

                foreach ($invoice->dish->products as $ingredient) {
                    Product::find($ingredient->product->id)->update([
                        'stock' => $ingredient->product->stock - ($ingredient->quantity * $invoice->quantity),
                    ]);
                }
            }
        }

        return $invoice['id'];
    }

    public function update(Request $request, $id)
    {
        $products = $request->products;

        $invoice = Invoice::find($id)->update([
            'paid_value' => $request['paid_value'],
            'invoice_status_id' => $request['status']
        ]);

        InvoiceProducts::where('invoice_id', '=', $id)->delete();

        foreach ($products as $product) {
            if (isset($product['brand'])) {
                InvoiceProducts::create([
                    'invoice_id' => $id,
                    'product_id' => $product['id'],
                    'quantity' => $product['quantity'],
                    'price' => $product['total'],
                ]);

                Product::find($product['id'])->update([
                    'stock' => $product['stock'] - $product['quantity'],
                ]);
            } else {
                $invoice = InvoiceProducts::create([
                    'invoice_id' => $id,
                    'dish_id' => $product['id'],
                    'quantity' => $product['quantity'],
                    'price' => $product['total'],
                ]);

                foreach ($invoice->dish->products as $ingredient) {
                    Product::find($ingredient->product->id)->update([
                        'stock' => $ingredient->product->stock - ($ingredient->quantity * $invoice->quantity),
                    ]);
                }
            }
        }

        return;
    }

    public function destroy($id)
    {
        InvoiceProducts::where('invoice_id', '=', $id)->delete();
        Invoice::find($id)->delete();
    }

    public function invoice($id)
    {

        $invoice = Invoice::with('products.product', 'status', 'customer.person')->find($id);

        $data = [
            'title' => 'Factura',
            'invoice' => $invoice,
        ];

        $data = \PDF::loadView('invoice', $data)
            ->setPaper('a4', '')
            ->save(public_path('tmp/') . 'cuenta_' . $invoice->id);

        return '/tmp/' . 'cuenta_' . $invoice->id;
    }
}
