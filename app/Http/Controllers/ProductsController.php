<?php

namespace App\Http\Controllers;

use App\Category;
use App\Dish;
use App\Product;
use App\Provider;
use App\ProductType;
use Illuminate\Http\Request;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class ProductsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $products = Product::all();
        $categories = Category::all();
        $providers = Provider::all();
        $types = ProductType::all();

        return view('products', compact('products', 'categories', 'providers', 'types'));
    }

    public function data()
    {
        $products = Product::with('category', 'provider')->get();
        return $products;
    }

    public function productList(Request $request)
    {
        $pages = (!isset($request['pages'])) ? 2 : 3;

        if ($request['name'])
        {
            $products = Product::Where([['name', 'LIKE', $request['name']."%"], ['stock', '>', 0], ['product_type_id', '=', '2']])->with('category', 'provider')->paginate($pages);
        }
        else
        {
            $products = Product::Where([['stock', '>', 0], ['product_type_id', '=', '2']])->with('category', 'provider')->paginate($pages);
        }

        return [
            'paginate' => [
                'total' => $products->total(),
                'current_page' => $products->currentPage(),
                'per_page' => $products->perPage(),
                'last_page' => $products->lastPage(),
                'from' => $products->firstItem(),
                'to' => $products->lastItem(),
            ],

            'products' => $products,
            ];
    }

    public function ingredients(Request $request)
    {
        if ($request['name'])
        {
            $products = Product::Where([['name', 'LIKE', $request['name']."%"], ['stock', '>', 0], ['product_type_id', '=', '1']])->with('category', 'provider')->paginate(2);
        }
        else
        {
            $products = Product::Where([['stock', '>', 0], ['product_type_id', '=', '1']])->with('category', 'provider')->paginate(2);
        }

        return [
            'paginate' => [
                'total' => $products->total(),
                'current_page' => $products->currentPage(),
                'per_page' => $products->perPage(),
                'last_page' => $products->lastPage(),
                'from' => $products->firstItem(),
                'to' => $products->lastItem(),
            ],

            'products' => $products,
        ];
    }

    public function nameSearch(Request $request)
    {
        $products = Product::Where('name', 'LIKE', $request['name']."%")->with('category', 'provider')->paginate(2);

        return [
            'paginate' => [
                'total' => $products->total(),
                'current_page' => $products->currentPage(),
                'per_page' => $products->perPage(),
                'last_page' => $products->lastPage(),
                'from' => $products->firstItem(),
                'to' => $products->lastItem(),
            ],

            'products' => $products,
        ];
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'brand' => 'required',
            'price' => 'required',
            'cost' => 'required',
            'stock' => 'required',
            'type' => 'required',
        ]);

        Product::create([
            'name' => $request['name'],
            'brand' => $request['brand'],
            'price' => $request['price'],
            'cost' => $request['cost'],
            'stock' => $request['stock'],
            'category_id' => $request['category'],
            'provider_id' => $request['provider'],
            'product_type_id' => $request['type'],
        ]);

        return;
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'brand' => 'required',
            'price' => 'required',
            'cost' => 'required',
            'stock' => 'required',
        ]);

        Product::find($id)->update([
            'name' => $request['name'],
            'brand' => $request['brand'],
            'price' => $request['price'],
            'cost' => $request['cost'],
            'stock' => $request['stock'],
            'category_id' => $request['category'],
            'provider_id' => $request['provider'],
            'product_type_id' => $request['type'],
        ]);

        return;
    }

    public function destroy($id)
    {
        $product = Product::find($id);
        $product->delete();
    }

    public function export()
    {
        $date = date('m_d_y');
        $url = public_path().'/tmp/products_'. $date .'.xlsx';

         $products = $this->data();

        $styleArray = [
            'borders' => [
                'outline' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '000000'],
                ],
            ],
        ];

        $headerStyle = [
            'font' => [
                'bold' => true,
            ],
            'borders' => [
                'outline' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => \PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE],
                ],
            ],
            'fill' => [
                'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                'startColor' => [
                    'argb' => \PhpOffice\PhpSpreadsheet\Style\Color::COLOR_BLACK,
                ],
            ]
        ];

        $row = 2;

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $columns = [
            'A' => 'ID',
            'B' => 'Nombre',
            'C' => 'Marca',
            'D' => 'Precio Venta',
            'E' => 'Precio Costo',
            'F' => 'Stock',
            'G' => 'Categoria',
            'H' => 'Proveedor',
        ];

        foreach ($columns as $column => $name)
        {
            $sheet->getColumnDimension($column)->setAutoSize(true);
            $sheet->setCellValue($column.'1', $name);
            $sheet->getStyle($column.'1')->applyFromArray($headerStyle);

            $sheet->getStyle($column.'1')->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
        }

        foreach ($products as $product){

            $sheet->setCellValue('A'.$row, $product->id);
            $sheet->getStyle('A'.$row)->applyFromArray($styleArray);

            $sheet->setCellValue('B'.$row, $product->name);
            $sheet->getStyle('B'.$row)->applyFromArray($styleArray);

            $sheet->setCellValue('C'.$row, $product->brand);
            $sheet->getStyle('C'.$row)->applyFromArray($styleArray);

            $sheet->setCellValue('D'.$row, $product->price);
            $sheet->getStyle('D'.$row)->applyFromArray($styleArray);

            $sheet->setCellValue('E'.$row, $product->cost);
            $sheet->getStyle('E'.$row)->applyFromArray($styleArray);

            $sheet->setCellValue('F'.$row, $product->stock);
            $sheet->getStyle('F'.$row)->applyFromArray($styleArray);

            $sheet->setCellValue('G'.$row, $product->category->name);
            $sheet->getStyle('G'.$row)->applyFromArray($styleArray);

            $sheet->setCellValue('H'.$row, $product->provider['name']);
            $sheet->getStyle('H'.$row)->applyFromArray($styleArray);

            $row += 1;
        }

        $writer = new Xlsx($spreadsheet);
        $writer->save($url);

        return '/tmp/products_'. $date .'.xlsx';
    }
}
