<?php

namespace App\Http\Controllers;

use App\Dish;
use App\DishIngredient;
use App\Invoice;
use App\InvoiceStatus;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DishesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $invoices = Invoice::all();
        $status = InvoiceStatus::all();
        
        return view('dishes', compact('invoices', 'status'));
    }

    public function data()
    {
        $dishes = Dish::with('products.product')->get();
        return $dishes;
    }

    public function dishesList(Request $request)
    {
        $pages = (!isset($request['pages'])) ? 2 : 3;

        if ($request['name'])
        {
            $dishes = Dish::Where([['name', 'LIKE', $request['name']."%"]])->with('products.product')->paginate($pages);
        }
        else
        {
            $dishes = Dish::with('products.product')->paginate($pages);
        }

        foreach ($dishes as $key => $dish)
        {
            foreach ($dish->products as $ingredient)
            {
                if ($ingredient->quantity > $ingredient->product->stock)
                {
                    unset($dishes[$key]);
                }
            }
        }

        return [
            'paginate' => [
                'total' => $dishes->total(),
                'current_page' => $dishes->currentPage(),
                'per_page' => $dishes->perPage(),
                'last_page' => $dishes->lastPage(),
                'from' => $dishes->firstItem(),
                'to' => $dishes->lastItem(),
            ],

            'dishes' => $dishes,
        ];
    }

    public function store(Request $request)
    {
        $ingredients = $request->ingredients;

        $dish = Dish::create([
            'name' => $request['name'],
            'description' => $request['description'],
            'price' => $request['price'],
            'cost' => $request['cost'],
        ]);

        foreach ($ingredients as $ingredient)
        {
            DishIngredient::create([
                'dish_id' => $dish['id'],
                'product_id' => $ingredient['id'],
                'quantity' => $ingredient['quantity'],
                'total' => $ingredient['total'],
            ]);
        }

        return;
    }

    public function update(Request $request, $id)
    {
        $ingredients = $request->ingredients;

        $dish = Dish::find ($id)->update([
            'name' => $request['name'],
            'description' => $request['description'],
            'price' => $request['price'],
            'cost' => $request['cost'],
        ]);

        DishIngredient::where('dish_id', '=', $id)->delete();

        foreach ($ingredients as $ingredient)
        {
            DishIngredient::create([
                'dish_id' => $id,
                'product_id' => $ingredient['id'],
                'quantity' => $ingredient['quantity'],
                'total' => $ingredient['total'],
            ]);
        }

        return;
    }

    public function destroy($id)
    {
        DishIngredient::where('dish_id', '=', $id)->delete();
        Dish::find($id)->delete();
    }
}
