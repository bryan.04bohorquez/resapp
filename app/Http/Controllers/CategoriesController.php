<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class CategoriesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $categories = Category::all();
        return view('categories', compact('categories'));
    }

    public function data()
    {
        $categories = Category::all();
        return $categories;
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'description' => 'required',
        ]);

        Category::create([
            'name' => $request['name'],
            'description' => $request['description']
        ]);

        return;
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'description' => 'required',
        ]);

        Category::find($id)->update([
            'name' => $request['name'],
            'description' => $request['description']
        ]);
    }

    public function destroy($id)
    {
        $category = Category::find($id);
        $category->delete();
    }

    public function export()
    {
        $date = date('m_d_y');
        $url = public_path().'/tmp/categories_'.$date.'.xlsx';

        $categories = $this->data();

        $styleArray = [
            'borders' => [
                'outline' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '000000'],
                ],
            ],
        ];

        $headerStyle = [
            'font' => [
                'bold' => true,
            ],
            'borders' => [
                'outline' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => \PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE],
                ],
            ],
            'fill' => [
                'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                'startColor' => [
                    'argb' => \PhpOffice\PhpSpreadsheet\Style\Color::COLOR_BLACK,
                ],
            ]
        ];

        $row = 2;

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $columns = [
            'A' => 'ID',
            'B' => 'Nombre',
            'C' => 'Descripción'
        ];

        foreach ($columns as $column => $name)
        {
            $sheet->getColumnDimension($column)->setAutoSize(true);
            $sheet->setCellValue($column.'1', $name);
            $sheet->getStyle($column.'1')->applyFromArray($headerStyle);
            $sheet->getStyle($column.'1')->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
        }

         foreach ($categories as $category){

             $sheet->setCellValue('A'.$row, $category->id);
             $sheet->getStyle('A'.$row)->applyFromArray($styleArray);

             $sheet->setCellValue('B'.$row, $category->name);
             $sheet->getStyle('B'.$row)->applyFromArray($styleArray);

             $sheet->setCellValue('C'.$row, $category->description);
             $sheet->getStyle('C'.$row)->applyFromArray($styleArray);

             $row += 1;
         }

        $spreadsheet->getActiveSheet()->getStyle('C1:C'.$row)->getAlignment()->setWrapText(true);

         $writer = new Xlsx($spreadsheet);
         $writer->save($url);

         return '/tmp/categories_' . $date . '.xlsx';
    }

}

