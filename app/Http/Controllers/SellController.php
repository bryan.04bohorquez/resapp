<?php

namespace App\Http\Controllers;

use App\Invoice;
use App\InvoiceStatus;
use Illuminate\Http\Request;

class SellController extends Controller
{
    public function index() {
        $invoices = Invoice::all();
        $status = InvoiceStatus::all();
        return view('sell', compact('invoices', 'status'));
    }
}
