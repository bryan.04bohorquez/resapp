<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $table = 'customers';

    protected $fillable = [
        'person_id',
    ];

    public function person()
    {
        return $this->belongsTo(Person::class);
    }
}
