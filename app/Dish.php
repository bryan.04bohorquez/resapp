<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dish extends Model
{
    protected $fillable = [
        'name',
        'description',
        'price',
        'cost',
    ];

    public function products()
    {
        return $this->hasMany(DishIngredient::class);
    }
}
