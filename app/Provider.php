<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Provider extends Model
{
    protected $table = 'providers';

    protected $fillable = [
        'name',
        'business_name',
        'address',
        'phone',
        'nit',
        'payment_method',
        'bank_account',
        'iva',
        'delivery_frequency',
        'type',
        'representant_id',
    ];

    public function representant()
    {
        return $this->belongsTo(Person::class);
    }

    public function products()
    {
        return $this->hasMany(Product::class);
    }
}
