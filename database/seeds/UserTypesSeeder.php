<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user_types = ['Administrador', 'Empleado'];

        foreach ($user_types as $user_type)
        {
            DB::table('user_types')->insert([
                'name' => $user_type,
            ]);
        }
    }
}
