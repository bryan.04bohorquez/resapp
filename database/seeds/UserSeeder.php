<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'username' => 'admin',
            'person_id' => 1,
            'user_type_id' => 1,
            'email' => 'admin@resapp.com',
            'password' => Hash::make('local'),
        ]);
    }
}
