<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class PersonSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('persons')->insert([
            'first_name' => 'Administrador',
            'last_name' => '',
            'identification' => '12345678',            
            'phone' => '12345678',
        ]);
    }
}
