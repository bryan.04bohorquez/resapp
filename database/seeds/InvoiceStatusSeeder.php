<?php

use Illuminate\Database\Seeder;

class InvoiceStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('invoice_status')->insert([
            'name' => 'activa',
        ]);

        DB::table('invoice_status')->insert([
            'name' => 'pagada',
        ]);
    }
}
