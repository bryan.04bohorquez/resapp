if (document.getElementById('dashboard')) {

    let dashboard = new Vue ({

        el: '#dashboard',

        data: {

        },

        methods: {

            inventario: function()
            {
                window.location.href = '/inventory'
            },

            users: function()
            {
                window.location.href = '/users'
            },

            products: function()
            {
                window.location.href = '/products'
            },

            providers: function()
            {
                window.location.href = '/providers'
            },

            customers: function()
            {
                window.location.href = '/customers'
            },

            invoices: function()
            {
                window.location.href = '/invoices'
            },

            dishes: function()
            {
                window.location.href = '/dishes'
            },

            categories: function()
            {
                window.location.href = '/categories'
            },

            sell: function()
            {
                window.location.href = '/sell'
            }

        },

        mounted() {

        }

    })

}

if (document.getElementById('categories')) {
    let categories = new Vue({

        el: '#categories',

        mounted() {
            this.getCategories();
        },

        data: {
            categories: [],

            modalTitle: 'Crear Categoria',

            category_id: '',
            name: '',
            description: '',

            isloading: true,

            errors: '',
        },

        methods: {

            getCategories: async function () {

                let url = '/categories/data';

                this.isloading = true;

                await axios.get(url)
                    .then(response => {
                        this.categories = response.data;
                        this.setDataTable();
                    });

                this.isloading = false;
            },

            openModal: function (type) {

                if (type == 'Create')
                {
                    this.cleanFields();
                    this.modalTitle = 'Crear Categoria';
                }

                $('#createCategory').modal('show');
            },

            closeModal: function () {
                this.cleanFields();
                $('#createCategory').modal('hide');
            },

            editModal: function (category) {
                this.category_id = category.id;
                this.modalTitle = "Editar Categoria" + category.name;

                this.name = category.name;
                this.description = category.description;

                this.openModal();
            },

            save: function () {
                if (this.category_id) {
                    this.editCategory(this.category_id);
                } else {
                    this.createCategory();
                }
            },

            createCategory: function () {
                let url = 'categories';

                axios.post(
                    url,
                    {
                        name: this.name,
                        description: this.description
                    }
                )
                    .then(response => {
                        $("#categoriesTable").DataTable().destroy();
                        this.getCategories();
                        this.closeModal();
                        toastr.success('Se agrego la categoria con exito');
                    })
                    .catch(error => {
                        this.errors = error.response.data.errors;
                    });
            },

            editCategory: function (id) {

                let url = '/categories/' + id;

                axios.put(
                    url,
                    {
                        name: this.name,
                        description: this.description,
                    }
                )
                    .then(response => {
                        $("#categoriesTable").DataTable().destroy();
                        this.getCategories();
                        this.closeModal();
                        toastr.success('Se actualizaron los datos de la categoria');
                    })
                    .catch(error => {
                        this.errors = error.response.data.errors;
                    });

            },

            deleteCategory: function (id) {

                var url = '/categories/' + id;

                Swal.fire({
                    title: '¿Esta seguro?',
                    text: "Esta acción no se puede revertir",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: 'black',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Si, eliminar registro',
                    cancelButtonText: 'No. conservarlo',
                }).then((result) => {
                    if (result.value) {
                        axios.delete(url)
                            .then(response => {
                                $("#categoriesTable").DataTable().destroy();
                                this.getCategories();
                                toastr.success('Eliminado correctamente');
                            })
                            .catch(error => {
                                toastr.error('Fallo al eliminar');
                            });
                    }
                })
            },

            setDataTable: function () {
                $(document).ready(function () {
                    $('#categoriesTable').DataTable({
                        "orderCellsTop": true,
                        "fixedHeader": true,
                        "language": {
                            "lengthMenu": "Mostrar _MENU_ registros",
                            "info": "Mostrando registros desde _START_ hasta _END_ de _TOTAL_ registros",
                            "infoFiltered": "(filtrado de _MAX_ entradas totales)",
                            "search": "Buscar:",
                            "zeroRecords": "No se encontraron registros coincidentes",
                            "paginate": {
                                "first": "Primero",
                                "last": "Ultimo",
                                "next": "Siguiente",
                                "previous": "Anterior"
                            },
                        },
                    });
                });
            },

            cleanFields: function () {
                this.category_id = '';
                this.name = '';
                this.description = '';
                this.errors = '';
            },

            exportExcel: function () {

                let url = '/categories/export';

                axios.get(
                    url,
                )
                    .then(response => {
                        window.open(response.data, '_blank');
                    })
                    .catch(error => {
                        console.log(error);
                    })

            }

        }
    });
}

if (document.getElementById('users')) {
    let users = new Vue({

        el: '#users',

        data: {
            users: [],
            user: {
                person: {
                    first_name: '',
                    last_name: '',
                    identification: '',
                    phone: '',
                },
                id: '',
                username: '',
                email: '',
                user_type: {
                    name: ''
                },
                updated_at: '',
            },

            modalTitle: 'Crear Usuario',
            labelContraseña: 'Contraseña',

            user_id: '',
            username: '',
            first_name: '',
            last_name: '',
            identification: '',
            user_type: '',
            phone: '',
            email: '',
            password: '',
            password_confirm: '',

            isloading: true,

            errors: '',
        },

        methods: {

            getUsers: async function () {

                var url = '/users/data';

                this.isloading = true;

                await axios.get(url)
                    .then(response => {
                        this.users = response.data;
                        this.setDataTable();
                    });

                this.isloading = false;

            },

            openModal: function (type) {

                if (type == 'Create') {
                    this.cleanFields();
                    this.modalTitle = 'Crear Usuario';
                    this.labelContraseña = 'Contraseña';
                }

                $('#createUser').modal('show');
            },

            closeModal: function () {
                this.cleanFields();
                $('#createUser').modal('hide');
            },

            closeDetail: function () {
                $('#detailUser').modal('hide');
            },

            editModal: function (user) {
                this.user_id = user.id;
                this.modalTitle = "Editar a " + user.person.first_name + " " + user.person.last_name;
                this.labelContraseña = "Nueva contraseña";

                this.username = user.username;
                this.first_name = user.person.first_name;
                this.last_name = user.person.last_name;
                this.identification = user.person.identification;
                this.user_type = user.user_type_id;
                this.phone = user.person.phone;
                ;
                this.email = user.email;

                this.openModal();
            },

            detailModal: function (user) {
                this.user = user;
                $('#detailUser').modal('show');
            },

            save: function () {
                if (this.user_id) {
                    this.editUser(this.user_id);
                } else {
                    this.createUser();
                }
            },

            createUser: function () {
                var url = 'users';

                axios.post(
                    url,
                    {
                        username: this.username,
                        first_name: this.first_name,
                        last_name: this.last_name,
                        identification: this.identification,
                        user_type: this.user_type,
                        phone: this.phone,
                        email: this.email,
                        password: this.password,
                    }
                )
                    .then(response => {
                        $("#usersTable").DataTable().destroy();
                        this.getUsers();
                        this.closeModal();
                        toastr.success('Se agrego el usuario con exito');
                    })
                    .catch(error => {
                        this.errors = error.response.data.errors;
                    });
            },

            editUser: function (id) {

                var url = '/users/' + id;

                axios.put(
                    url,
                    {
                        username: this.username,
                        first_name: this.first_name,
                        last_name: this.last_name,
                        identification: this.identification,
                        user_type: this.user_type,
                        phone: this.phone,
                        email: this.email,
                        password: this.password,
                    }
                )
                    .then(response => {
                        $("#usersTable").DataTable().destroy();
                        this.getUsers();
                        this.closeModal();
                        toastr.success('Se actualizaron los datos del usuario');
                    })
                    .catch(error => {
                        this.errors = error.response.data.errors;
                    });

            },

            deleteUser: function (id) {

                var url = '/users/' + id;

                Swal.fire({
                    title: '¿Esta seguro?',
                    text: "Esta acción no se puede revertir",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: 'black',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Si, eliminar registro',
                    cancelButtonText: 'No. conservarlo',
                }).then((result) => {
                    if (result.value) {
                        axios.delete(url)
                            .then(response => {
                                $("#usersTable").DataTable().destroy();
                                this.getUsers();
                                toastr.success('Eliminado correctamente');
                            })
                            .catch(error => {
                                toastr.error('Fallo al eliminar');
                            });
                    }
                })
            },

            setDataTable: function () {
                $(document).ready(function () {
                    $('#usersTable').DataTable({
                        "orderCellsTop": true,
                        "fixedHeader": true,
                        "language": {
                            "lengthMenu": "Mostrar _MENU_ registros",
                            "info": "Mostrando registros desde _START_ hasta _END_ de _TOTAL_ registros",
                            "infoFiltered": "(filtrado de _MAX_ entradas totales)",
                            "search": "Buscar:",
                            "zeroRecords": "No se encontraron registros coincidentes",
                            "paginate": {
                                "first": "Primero",
                                "last": "Ultimo",
                                "next": "Siguiente",
                                "previous": "Anterior"
                            },
                        },
                    });
                });
            },

            cleanFields: function () {
                this.user_id = '';
                this.username = '';
                this.first_name = '';
                this.last_name = '';
                this.identification = '';
                this.user_type = '';
                this.phone = '';
                this.email = '';
                this.password = '';
                this.password_confirm = '';

                this.errors = '';
            },

            exportExcel: function () {

                let url = '/users/export';

                axios.get(
                    url,
                )
                    .then(response => {
                        window.open(response.data, '_blank');
                    })
                    .catch(error => {
                        console.log(error);
                    })

            }

        },

        mounted() {
            this.getUsers();
        },
    });
}

if (document.getElementById('products')) {
    let products = new Vue({

        el: '#products',

        mounted() {
            this.getProducts();
        },

        data: {
            products: [],
            product: {
                id: '',
                name: '',
                brand: '',
                provider: {
                    name: '',
                },
                price: '',
                stock: '',
                category: {
                    name: '',
                },
                type: {
                    name:'',
                },
                created_at: '',
                updated_at: '',
            },

            modalTitle: 'Crear Producto',

            product_id: '',
            name: '',
            brand: '',
            price: '',
            iva_price: 0,
            cost: '',
            iva_cost: 0,
            stock: '',
            category: '',
            provider: '',
            type: '',

            isloading: true,

            errors: '',
        },

        methods: {

            getProducts: async function () {

                var url = '/products/data';

                this.isloading = true;

                await axios.get(url)
                    .then(response => {
                        this.products = response.data;
                        this.setDataTable();
                    });

                this.isloading = false;
            },

            openModal: function (type) {

                if (type == 'Create') {
                    this.cleanFields();
                    this.modalTitle = 'Crear Producto';
                }

                $('#createProduct').modal('show');
            },

            closeModal: function () {
                this.cleanFields();
                $('#createProduct').modal('hide');
            },

            closeDetail: function () {
                $('#detailProduct').modal('hide');
            },

            editModal: function (product) {
                this.product_id = product.id;
                this.modalTitle = "Editar " + product.name;

                this.name = product.name;
                this.brand = product.brand;
                this.price = product.price;
                this.cost = product.cost;
                this.stock = product.stock;
                this.category = product.category_id;
                this.provider = product.provider_id;
                this.type = product.product_type_id;

                this.openModal();
            },

            detailModal: function (product) {
                this.product = product;
                $('#detailProduct').modal('show');
            },

            save: function () {
                if (this.product_id) {
                    this.editProduct(this.product_id);
                } else {
                    this.createProduct();
                }
            },

            createProduct: function () {
                var url = 'products';

                axios.post(
                    url,
                    {
                        name: this.name,
                        brand: this.brand,
                        price: parseInt(this.price) + ((this.iva_price / 100) * this.price),
                        cost: parseInt(this.cost) + ((this.iva_cost / 100) * this.cost),
                        stock: this.stock,
                        category: this.category,
                        provider: this.provider,
                        type: this.type,
                    }
                )
                    .then(response => {
                        $("#productsTable").DataTable().destroy();
                        this.getProducts();
                        this.closeModal();
                        toastr.success('Se agrego el producto con exito');
                    })
                    .catch(error => {
                        this.errors = error.response.data.errors;
                    });
            },

            editProduct: function (id) {

                var url = '/products/' + id;

                axios.put(
                    url,
                    {
                        name: this.name,
                        brand: this.brand,
                        price: parseInt(this.price) + ((this.iva_price / 100) * this.price),
                        cost: parseInt(this.cost) + ((this.iva_cost / 100) * this.cost),
                        stock: this.stock,
                        category: this.category,
                        provider: this.provider,
                        type: this.type,
                    }
                )
                    .then(response => {
                        $("#productsTable").DataTable().destroy();
                        this.getProducts();
                        this.closeModal();
                        toastr.success('Se actualizaron los datos del producto');
                    })
                    .catch(error => {
                        this.errors = error.response.data.errors;
                    });

            },

            deleteProduct: function (id) {

                var url = '/products/' + id;

                Swal.fire({
                    title: '¿Esta seguro?',
                    text: "Esta acción no se puede revertir",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: 'black',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Si, eliminar registro',
                    cancelButtonText: 'No. conservarlo',
                }).then((result) => {
                    if (result.value) {
                        axios.delete(url)
                            .then(response => {
                                $("#productsTable").DataTable().destroy();
                                this.getProducts();
                                toastr.success('Eliminado correctamente');
                            })
                            .catch(error => {
                                toastr.error('Fallo al eliminar');
                            });
                    }
                })
            },

            setDataTable: function () {
                $(document).ready(function () {
                    $('#productsTable').DataTable({
                        "orderCellsTop": true,
                        "fixedHeader": true,
                        "language": {
                            "lengthMenu": "Mostrar _MENU_ registros",
                            "info": "Mostrando registros desde _START_ hasta _END_ de _TOTAL_ registros",
                            "infoFiltered": "(filtrado de _MAX_ entradas totales)",
                            "search": "Buscar:",
                            "zeroRecords": "No se encontraron registros coincidentes",
                            "paginate": {
                                "first": "Primero",
                                "last": "Ultimo",
                                "next": "Siguiente",
                                "previous": "Anterior"
                            },
                        },
                    });
                });
            },

            cleanFields: function () {
                this.product_id = '';
                this.name = '';
                this.brand = '';
                this.price = '';
                this.iva_price = '';
                this.cost = '';
                this.iva_cost = '';
                this.stock = '';
                this.category = '';
                this.provider = '';
                this.type = '';

                this.errors = '';
            },

            exportExcel: function () {

                let url = '/products/export';

                axios.get(
                    url,
                )
                    .then(response => {
                        window.open(response.data, '_blank');
                    })
                    .catch(error => {
                        console.log(error);
                    })

            }

        }
    });
}

if (document.getElementById('providers')) {
    let providers = new Vue({

        el: '#providers',

        data: {
            providers: [],
            provider: {
                name: '',
                business_name: '',
                address: '',
                nit: '',
                payment_method: '',
                bank_account: '',
                iva: '',
                delivery_frequency: '',
            },

            modalTitle: 'Crear Proveedor',
            crudTitle: 'Datos de la Empresa',

            //Provider Data
            provider_id: '',
            name: '',
            business_name: '',
            address: '',
            phone: '',
            nit: '',
            payment_method: '',
            bank_account: '',
            iva: '',
            delivery_frequency: '',

            //Representant Data
            type: '',
            p_first_name: '',
            p_last_name: '',
            p_identification: '',
            p_phone: '',

            isloading: true,

            errors: [],
        },

        methods: {

            getProviders: async function () {

                var url = '/providers/data';

                this.isloading = true;

                await axios.get(url)
                    .then(response => {
                        this.providers = response.data;
                        this.setDataTable();
                    });

                this.isloading = false;

            },

            openModal: function (type) {

                if (type == 'Create')
                {
                    this.cleanFields();

                    Swal.fire({
                        title: 'Tipo de Proveedor',
                        text: "¿Este proveedor es una empresa o una persona?",
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: 'black',
                        cancelButtonColor: 'black',
                        confirmButtonText: 'Empresa',
                        cancelButtonText: 'Persona',
                    }).then((result) => {
                        if (result.value) {

                            this.type = 'enterprice';
                            this.crudTitle = 'Datos de la Empresa';
                            $('#createProvider').modal('show');
                        }
                        else
                        {
                            this.type = 'person';
                            this.crudTitle = 'Datos de la Persona',

                            $('#createProvider').modal('show');
                        }
                    })
                }
                else
                {
                    $('#createProvider').modal('show');
                }
            },

            closeModal: function () {
                this.cleanFields();
                $('#createProvider').modal('hide');
            },

            closeDetail: function () {
                $('#detailProvider').modal('hide');
            },

            editModal: function (provider) {

                this.provider_id = provider.id;
                this.modalTitle = "Editar " + provider.name;

                this.type = provider.type;
                this.name = provider.name;
                this.business_name = provider.business_name;
                this.address = provider.address;
                this.phone = provider.phone;
                this.nit = provider.nit;
                this.payment_method = provider.payment_method;
                this.bank_account = provider.bank_account;
                this.iva = provider.iva;
                this.delivery_frequency = provider.delivery_frequency;

                this.p_first_name = provider.representant ? provider.representant.first_name : '';
                this.p_last_name = provider.representant ? provider.representant.last_name : '';
                this.p_identification = provider.representant ? provider.representant.identification : '';
                this.p_phone = provider.representant ? provider.representant.phone : '';

                this.openModal();
            },

            detailModal: function (provider) {
                this.provider = provider;
                this.type = provider.type;
                $('#detailProvider').modal('show');
            },

            save: function () {
                if (this.provider_id) {
                    this.editProvider(this.provider_id);
                } else {
                    this.createProvider();
                }
            },

            createProvider: function () {
                var url = 'providers';

                axios.post(
                    url,
                    {
                        //Provider Data
                        type: this.type,
                        name: this.name,
                        business_name: this.business_name,
                        address: this.address,
                        phone: this.phone,
                        nit: this.nit,
                        payment_method: this.payment_method,
                        bank_account: this.bank_account,
                        iva: this.iva,
                        delivery_frequency: this.delivery_frequency,

                        //Representant Data
                        p_first_name: this.p_first_name,
                        p_last_name: this.p_last_name,
                        p_identification: this.p_identification,
                        p_phone: this.p_phone,
                    }
                )
                    .then(response => {
                        $("#providersTable").DataTable().destroy();
                        this.getProviders();
                        this.closeModal();
                        toastr.success('Se agrego el proveedor con exito');
                    })
                    .catch(error => {
                        this.errors = error.response.data.errors;
                    });
            },

            editProvider: function (id) {

                var url = '/providers/' + id;

                axios.put(
                    url,
                    {
                        name: this.name,
                        business_name: this.business_name,
                        address: this.address,
                        phone: this.phone,
                        nit: this.nit,
                        payment_method: this.payment_method,
                        bank_account: this.bank_account,
                        iva: this.iva,
                        delivery_frequency: this.delivery_frequency,

                        p_first_name: this.p_first_name,
                        p_last_name: this.p_last_name,
                        p_identification: this.p_identification,
                        p_phone: this.p_phone,
                    }
                )
                    .then(response => {
                        $("#providersTable").DataTable().destroy();
                        this.getProviders();
                        this.closeModal();
                        toastr.success('Se actualizaron los datos del proveedor');
                    })
                    .catch(error => {
                        this.errors = error.response.data.errors;
                    });

            },

            deleteProvider: function (id) {

                var url = '/providers/' + id;

                Swal.fire({
                    title: '¿Esta seguro?',
                    text: "Esta acción no se puede revertir",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: 'black',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Si, eliminar registro',
                    cancelButtonText: 'No. conservarlo',
                }).then((result) => {
                    if (result.value) {
                        axios.delete(url)
                            .then(response => {
                                $("#providersTable").DataTable().destroy();
                                this.getProviders();
                                toastr.success('Eliminado correctamente');
                            })
                            .catch(error => {
                                toastr.error('Fallo al eliminar');
                            });
                    }
                })
            },

            setDataTable: function () {
                $(document).ready(function () {
                    $('#providersTable').DataTable({
                        "orderCellsTop": true,
                        "fixedHeader": true,
                        "language": {
                            "lengthMenu": "Mostrar _MENU_ registros",
                            "info": "Mostrando registros desde _START_ hasta _END_ de _TOTAL_ registros",
                            "infoFiltered": "(filtrado de _MAX_ entradas totales)",
                            "search": "Buscar:",
                            "zeroRecords": "No se encontraron registros coincidentes",
                            "paginate": {
                                "first": "Primero",
                                "last": "Ultimo",
                                "next": "Siguiente",
                                "previous": "Anterior"
                            },
                        },
                    });
                });
            },

            cleanFields: function () {
                this.provider_id = '';
                this.name = '';
                this.business_name = '';
                this.address = '';
                this.phone = '';
                this.nit = '';
                this.payment_method = '';
                this.bank_account = '';
                this.iva = '';
                this.delivery_frequency = '';

                this.p_first_name = '';
                this.p_last_name = '';
                this.p_identification = '';
                this.p_phone = '';

                this.errors = '';
            },

            exportExcel: function () {

                let url = '/providers/export';

                axios.get(
                    url,
                )
                    .then(response => {
                        window.open(response.data, '_blank');
                    })
                    .catch(error => {
                        console.log(error);
                    })

            }

        },

        mounted() {
            this.getProviders();
        },
    });
}

if (document.getElementById('customers')) {
    let users = new Vue({

        el: '#customers',

        data: {
            customers: [],
            customer: {
                person: {
                    first_name: '',
                    last_name: '',
                    identification: '',
                    phone: '',
                },
                id: '',
                updated_at: '',
            },

            modalTitle: 'Crear Cliente',

            customer_id: '',
            first_name: '',
            last_name: '',
            identification: '',
            phone: '',

            edit: false,

            isloading: true,

            errors: [],
        },

        methods: {

            getCustomers: async function () {

                var url = '/customers/data';

                this.isloading = true;

                await axios.get(url)
                    .then(response => {
                        this.customers = response.data;
                        this.setDataTable();
                    });

                this.isloading = false;
            },

            openModal: function (type) {

                if (type == 'Create') {
                    this.cleanFields();
                    this.modalTitle = 'Crear Cliente';
                }

                $('#createCustomer').modal('show');
            },

            closeModal: function () {
                this.cleanFields();
                $('#createCustomer').modal('hide');
            },

            closeDetail: function () {
                $('#detailCustomer').modal('hide');
            },

            editModal: function (customer) {
                this.customer_id = customer.id;
                this.modalTitle = "Editar a " + customer.person.first_name + " " + customer.person.last_name;

                this.first_name = customer.person.first_name;
                this.last_name = customer.person.last_name;
                this.identification = customer.person.identification;
                this.phone = customer.person.phone;

                this.openModal();
            },

            detailModal: function (customer) {
                this.customer = customer;
                $('#detailCustomer').modal('show');
            },

            save: function () {
                if (this.customer_id) {
                    this.editCustomer(this.customer_id);
                } else {
                    this.createCustomer();
                }
            },

            createCustomer: function () {
                var url = 'customers';

                axios.post(
                    url,
                    {
                        first_name: this.first_name,
                        last_name: this.last_name,
                        identification: this.identification,
                        phone: this.phone,
                    }
                )
                    .then(response => {
                        $("#customersTable").DataTable().destroy();
                        this.getCustomers();
                        this.closeModal();
                        toastr.success('Se agrego el cliente con exito');
                    })
                    .catch(error => {
                        this.errors = error.response.data;
                    });
            },

            editCustomer: function (id) {

                var url = '/customers/' + id;

                axios.put(
                    url,
                    {
                        first_name: this.first_name,
                        last_name: this.last_name,
                        identification: this.identification,
                        phone: this.phone,
                    }
                )
                    .then(response => {
                        $("#customersTable").DataTable().destroy();
                        this.getCustomers();
                        this.closeModal();
                        toastr.success('Se actualizaron los datos del usuario');
                    })
                    .catch(error => {
                        this.errors = error.response.data;
                    });

            },

            deleteCustomer: function (id) {

                var url = '/customers/' + id;

                Swal.fire({
                    title: '¿Esta seguro?',
                    text: "Esta acción no se puede revertir",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: 'black',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Si, eliminar registro',
                    cancelButtonText: 'No. conservarlo',
                }).then((result) => {
                    if (result.value) {
                        axios.delete(url)
                            .then(response => {
                                $("#customersTable").DataTable().destroy();
                                this.getCustomers();
                                toastr.success('Eliminado correctamente');
                            })
                            .catch(error => {
                                toastr.error('Fallo al eliminar');
                            });
                    }
                })
            },

            setDataTable: function () {
                $(document).ready(function () {
                    $('#customersTable').DataTable({
                        "orderCellsTop": true,
                        "fixedHeader": true,
                        "language": {
                            "lengthMenu": "Mostrar _MENU_ registros",
                            "info": "Mostrando registros desde _START_ hasta _END_ de _TOTAL_ registros",
                            "infoFiltered": "(filtrado de _MAX_ entradas totales)",
                            "search": "Buscar:",
                            "zeroRecords": "No se encontraron registros coincidentes",
                            "paginate": {
                                "first": "Primero",
                                "last": "Ultimo",
                                "next": "Siguiente",
                                "previous": "Anterior"
                            },
                        },
                    });
                });
            },

            cleanFields: function () {
                this.customer_id = '';
                this.first_name = '';
                this.last_name = '';
                this.identification = '';
                this.phone = '';
            },

            exportExcel: function () {

                let url = '/customers/export';

                axios.get(
                    url,
                )
                    .then(response => {
                        window.open(response.data, '_blank');
                    })
                    .catch(error => {
                        console.log(error);
                    })

            }

        },

        mounted() {
            this.getCustomers();
        },
    });
}

if (document.getElementById('invoices')) {
    let invoices = new Vue({

        el: '#invoices',

        data: {

            start_date: '',
            end_date: '',

            invoiceId: '',
            invoices: '',

            searchCustomer: '',

            //Client Data
            customer_id: false,
            first_name: '',
            last_name: '',
            identification: '',
            phone: '',

            //Account
            status: '',
            paid_total: 0,
            accountProducts: [],

            //Products
            isLoading: false,
            products: [],
            searchName: '',

            //Dishes
            dishes: [],
            searchDish: '',

            productsPagination: {
                total: 0,
                current_page: 0,
                per_page: 0,
                last_page: 0,
                from: 0,
                to: 0,
            },

            dishesPagination: {
                total: 0,
                current_page: 0,
                per_page: 0,
                last_page: 0,
                from: 0,
                to: 0,
            },

            offset: 3,

            edit: false,

            loading: true,

            errors: [],
        },

        computed: {

            isActived: function (pagination)
            {
                return this.productsPagination.current_page;
            },

            isActivedDishes: function (pagination)
            {
                return this.dishesPagination.current_page;
            },

            pagesNumber: function ()
            {
                if (!this.productsPagination.to)
                {
                    return [];
                }

                let from = this.productsPagination.current_page - this.offset;

                if (from < 1){
                    from = 1;
                }

                let to = from + (this.offset * 2);

                if (to >= this.productsPagination.last_page)
                {
                    to = this.productsPagination.last_page;
                }

                let pagesArray = [];

                while (from <= to)
                {
                    pagesArray.push(from);
                    from ++;
                }

                return pagesArray;
            },

            pagesNumberDishes: function ()
            {
                if (!this.dishesPagination.to)
                {
                    return [];
                }

                let from = this.dishesPagination.current_page - this.offset;

                if (from < 1){
                    from = 1;
                }

                let to = from + (this.offset * 2);

                if (to >= this.dishesPagination.last_page)
                {
                    to = this.dishesPagination.last_page;
                }

                let pagesArray = [];

                while (from <= to)
                {
                    pagesArray.push(from);
                    from ++;
                }

                return pagesArray;
            },
        },

        watch: {

            accountProducts: function()
            {
                this.paid_total = 0;

                this.accountProducts.forEach(product => {
                    console.log(product.total);
                    this.paid_total += parseInt(product.total);
                    console.log(this.paid_total);
                });
            },
        },

        methods: {

            getInvoices: async function () {
                var url = '/invoices/data';

                this.loading = true;

                await axios.get(url)
                    .then(response => {
                        this.invoices = response.data;
                        this.setDataTable();
                    });

                this.loading = false;
            },

            openModal: function (type) {
                $('#createInvoice').modal('show');
            },

            openReport: function () {
                $('#sellReport').modal('show');
            },

            genReport: function () {
                const url = '/invoices/report';

                axios.post(
                    url,
                    {
                        start_date: this.start_date,
                        end_date: this.end_date,
                    }
                ).
                    then( response => {
                        window.open(response.data, '_blank');
                })
                    .catch( error => {

                    });
            },

            closeReport: function () {
                $('#sellReport').modal('hide');
            },

            closeModal: function () {
                this.edit = false;
                this.searchName = '';
                this.cleanFields();
                $('#createInvoice').modal('hide');
            },

            getProducts: async function (page) {
                try {

                    this.isLoading = true;

                    let url = '/products/list';

                    if (!page)
                    {
                        page = 1;
                    }

                    await axios.post(
                        url,
                        {
                            page: page,
                            name: this.searchName,
                        })
                        .then(response => {
                            this.products = response.data.products.data;
                            this.productsPagination = response.data.paginate
                        });
                }
                finally {
                    this.isLoading = false;
                }
            },

            getDishes: async function (page) {
                try {

                    this.isLoading = true;

                    let url = '/dishes/list';

                    if (!page)
                    {
                        page = 1;
                    }

                    await axios.post(
                        url,
                        {
                            page: page,
                            name: this.searchName,
                        })
                        .then(response => {
                            this.dishes = response.data.dishes.data;
                            this.dishesPagination = response.data.paginate
                        });
                }
                finally {
                    this.isLoading = false;
                }
            },

            changePage: function (page) {
                 this.productsPagination.current_page = page;
                 this.getProducts(page);
            },

            changePageD: function (page) {
                this.dishesPagination.current_page = page;
                this.getDishes(page);
            },

            addAccount: function (product) {
                this.accountProducts.push(product);
            },

            deleteAccount: function (product) {

                for (let i = 0; i < this.accountProducts.length ; i++) {
                    if (this.accountProducts[i].name == product.name)
                    {
                        this.accountProducts.splice(i, 1);
                    }
                }

            },

            createInvoice: function () {
                var url = 'invoices';

                axios.post(
                    url,
                    {
                        //Client Data
                        customer_id: this.customer_id,
                        first_name: this.first_name,
                        last_name: this.last_name,
                        identification: this.identification,
                        phone: this.phone,

                        //Invoice Data
                        status: this.status,
                        paid_value: this.paid_total,

                        //Products
                        products: this.accountProducts,
                    }
                )
                    .then(response => {
                        $("#invoicesTable").DataTable().destroy();
                        this.getInvoices();
                        this.closeModal();
                        toastr.success('Se creo la cuenta con exito');
                    })
                    .catch(error => {
                        this.errors = error.response.data;
                    });
            },

            editInvoice: function (invoice) {

                this.invoiceId = invoice.id;
                this.edit = true;
                this.searchCustomer = invoice.customer.person.identification;
                this.getCustomer();
                this.status = invoice.invoice_status_id;

                invoice.products.forEach(product => {

                    let p = '';

                    if (!product.dish_id)
                    {
                        p = {
                            id: product.product.id,
                            name: product.product.name,
                            price: product.product.price,
                            quantity: product.quantity,
                            total: product.price,
                        }
                    }
                    else
                    {
                         p = {
                            id: product.dish.id,
                            name: product.dish.name,
                            price: product.dish.price,
                            quantity: product.quantity,
                            total: product.price,
                        }
                    }

                    this.accountProducts.push(p);
                });

                this.openModal();
            },

            updateInvoice: function () {

                var url = '/invoices/' + this.invoiceId;

                axios.put(
                    url,
                    {
                        //Invoice Data
                        status: this.status,
                        paid_value: this.paid_total,

                        //Products
                        products: this.accountProducts,
                    }
                )
                    .then(response => {
                        $("#invoicesTable").DataTable().destroy();
                        this.getInvoices();
                        this.closeModal();
                        toastr.success('Se actualizaron los datos de la cuenta');
                    })
                    .catch(error => {
                        this.errors = error.response.data;
                    });
            },

            deleteInvoice: function (id) {

                var url = '/invoices/' + id;

                Swal.fire({
                    title: '¿Esta seguro?',
                    text: "Esta acción no se puede revertir",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: 'black',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Si, eliminar registro',
                    cancelButtonText: 'No. conservarlo',
                }).then((result) => {
                    if (result.value) {
                        axios.delete(url)
                            .then(response => {
                                $("#invoicesTable").DataTable().destroy();
                                this.getInvoices();
                                toastr.success('Eliminado correctamente');
                            })
                            .catch(error => {
                                toastr.error('Fallo al eliminar');
                            });
                    }
                })
            },

            generateInvoice: function (id) {
                var url = '/invoice/pdf/' + id;

                axios.get(url)
                    .then(response => {
                        let win = window.open(response.data, '_blank');
                        win.focus();
                    });
            },

            getCustomer: function () {
                let url = '/customers/namesearch';

                axios.post(
                    url,
                    {
                        name: this.searchCustomer,
                    }
                )
                    .then(response => {

                        this.customer_id = (response.data[0]) ? response.data[0].customer_id : false;
                        this.first_name = (response.data[0]) ? response.data[0].first_name : '';
                        this.last_name = (response.data[0]) ? response.data[0].last_name : '';
                        this.identification = (response.data[0]) ? response.data[0].identification : '';
                        this.phone = (response.data[0]) ? response.data[0].phone : '';

                    })
            },

            setDataTable: function () {
                $(document).ready(function () {
                    $('#invoicesTable').DataTable({
                        "orderCellsTop": true,
                        "fixedHeader": true,
                        "language": {
                            "lengthMenu": "Mostrar _MENU_ registros",
                            "info": "Mostrando registros desde _START_ hasta _END_ de _TOTAL_ registros",
                            "infoFiltered": "(filtrado de _MAX_ entradas totales)",
                            "search": "Buscar:",
                            "zeroRecords": "No se encontraron registros coincidentes",
                            "paginate": {
                                "first": "Primero",
                                "last": "Ultimo",
                                "next": "Siguiente",
                                "previous": "Anterior"
                            },
                        },
                    });
                })
            },

            cleanFields: function () {

                    this.searchCustomer = '';

                    //Client Data
                    this.customer_id = false;
                    this.first_name = '';
                    this.last_name = '';
                    this.identification = '';
                    this.phone = '';

                    //Account
                    this.status = '';
                    this.paid_total = 0;
                    this.accountProducts = [];

                    //Products
                    this.searchName = '';

            },

        },

        mounted() {

            $("#startDate").datepicker({
                todayBtn: "linked",
                language: "es",
                autoclose: true
            }).on(
                "changeDate", () => {this.start_date = $('#startDate').val()});

            $("#endDate").datepicker({
                todayBtn: "linked",
                language: "es",
                autoclose: true
            }).on(
                "changeDate", () => {this.end_date = $('#endDate').val()});


            this.getInvoices();
            this.getProducts();
            this.getDishes();
        },
    });
}

if (document.getElementById('dishes')) {

    let dishes = new Vue({

        el: '#dishes',

        data: {

            dishId: '',
            dishes: '',

            name: '',
            description: '',
            price: '',
            cost: '',
            dishIngredients: [],

            //Ingredients
            isLoading: false,
            ingredients: [],
            searchName: '',

            ingredientsPagination: {
                total: 0,
                current_page: 0,
                per_page: 0,
                last_page: 0,
                from: 0,
                to: 0,
            },

            offset: 3,

            edit: false,

            loading: true,

            errors: [],
        },

        computed: {

            isActived: function ()
            {
                return this.ingredientsPagination.current_page;
            },

            pagesNumber: function ()
            {
                if (!this.ingredientsPagination.to)
                {
                    return [];
                }

                let from = this.ingredientsPagination.current_page - this.offset;

                if (from < 1){
                    from = 1;
                }

                let to = from + (this.offset * 2);

                if (to >= this.ingredientsPagination.last_page)
                {
                    to = this.ingredientsPagination.last_page;
                }

                let pagesArray = [];

                while (from <= to)
                {
                    pagesArray.push(from);
                    from ++;
                }

                return pagesArray;
            },

        },

        watch: {

            dishIngredients: function()
            {
                this.cost = 0;

                this.dishIngredients.forEach(product => {
                    console.log(product.total);
                    this.cost += parseInt(product.total);
                });
            },
        },

        methods: {

            getDishes: async function () {
                var url = '/dishes/data';

                this.loading = true;

                await axios.get(url)
                    .then(response => {
                        this.dishes = response.data;
                        this.setDataTable();
                    });

                this.loading = false;
            },

            openModal: function (type) {
                $('#createDish').modal('show');
            },

            closeModal: function () {
                this.edit = false;
                this.searchName = '';
                this.cleanFields();
                $('#createDish').modal('hide');
            },

            getProducts: async function (page) {
                try {

                    this.isLoading = true;

                    let url = '/products/ingredients';

                    if (!page)
                    {
                        page = 1;
                    }

                    await axios.post(
                        url,
                        {
                            page: page,
                            name: this.searchName,
                        })
                        .then(response => {
                            this.ingredients = response.data.products.data;
                            this.ingredientsPagination = response.data.paginate
                        });
                }
                finally {
                    this.isLoading = false;
                }
            },

            changePage: function (page) {
                this.ingredientsPagination.current_page = page;
                this.getProducts(page);
            },

            addIngredient: function (product) {

                let p = Object.assign({}, product);
                p.total = p.price * p.quantity;

                this.dishIngredients.push(p);
            },

            deleteIngredient: function (product) {

                for (let i = 0; i < this.dishIngredients.length ; i++) {
                    if (this.dishIngredients[i].name == product.name)
                    {
                        this.dishIngredients.splice(i, 1);
                    }
                }

            },

            createDish: function () {
                var url = 'dishes';

                axios.post(
                    url,
                    {
                        name: this.name,
                        description: this.description,
                        price: this.price,
                        cost: this.cost,
                        ingredients: this.dishIngredients,
                    }
                )
                    .then(response => {
                        $("#dishesTable").DataTable().destroy();
                        this.getDishes();
                        this.closeModal();
                        toastr.success('Se creo el plato con exito');
                    })
                    .catch(error => {
                        this.errors = error.response.data;
                    });
            },

            editDish: function (dish) {

                this.dishId = dish.id;
                this.edit = true;

                this.name = dish.name;
                this.description = dish.description;
                this.price = dish.price;
                this.cost = dish.cost;

                dish.products.forEach(product => {
                    let p = {
                        id: product.product.id,
                        name: product.product.name,
                        price: product.product.price,
                        quantity: product.quantity,
                        total: product.total,
                    }

                    this.dishIngredients.push(p);
                });

                this.openModal();
            },

            updateDish: function () {

                var url = '/dishes/' + this.dishId;

                axios.put(
                    url,
                    {
                        name: this.name,
                        description: this.description,
                        price: this.price,
                        cost: this.cost,
                        ingredients: this.dishIngredients,

                    }
                )
                    .then(response => {
                        $("#dishesTable").DataTable().destroy();
                        this.getDishes();
                        this.closeModal();
                        toastr.success('Se actualizaron los datos del plato');
                    })
                    .catch(error => {
                        this.errors = error.response.data;
                    });
            },

            deleteDish: function (id) {

                var url = '/dishes/' + id;

                Swal.fire({
                    title: '¿Esta seguro?',
                    text: "Esta acción no se puede revertir",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: 'black',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Si, eliminar registro',
                    cancelButtonText: 'No. conservarlo',
                }).then((result) => {
                    if (result.value) {
                        axios.delete(url)
                            .then(response => {
                                $("#dishesTable").DataTable().destroy();
                                this.getDishes();
                                toastr.success("Eliminado Correctamente");
                            })
                            .catch(error => {
                                console.log(error);
                                toastr.error('Fallo al eliminar');
                            });
                    }
                })
            },

            generateInvoice: function (id) {
                var url = '/invoice/pdf/' + id;

                axios.get(url)
                    .then(response => {
                        let win = window.open(response.data, '_blank');
                        win.focus();
                    });
            },

            setDataTable: function () {
                $(document).ready(function () {
                    $('#dishesTable').DataTable({
                        "orderCellsTop": true,
                        "fixedHeader": true,
                        "language": {
                            "lengthMenu": "Mostrar _MENU_ registros",
                            "info": "Mostrando registros desde _START_ hasta _END_ de _TOTAL_ registros",
                            "infoFiltered": "(filtrado de _MAX_ entradas totales)",
                            "search": "Buscar:",
                            "zeroRecords": "No se encontraron registros coincidentes",
                            "paginate": {
                                "first": "Primero",
                                "last": "Ultimo",
                                "next": "Siguiente",
                                "previous": "Anterior"
                            },
                        },
                    });
                })
            },

            cleanFields: function () {

                this.name = '';
                this.description = '';
                this.price = '';
                this.cost = '';
                this.dishIngredients = [];

            },

        },

        mounted() {
            this.getDishes();
            this.getProducts();
        },
    });
}

if (document.getElementById('sell')) {
    let invoices = new Vue({

        el: '#sell',

        data: {

            invoiceId: '',
            invoices: '',

            searchCustomer: '',

            //Client Data
            customer_id: false,
            first_name: '',
            last_name: '',
            identification: '',
            phone: '',

            //Account
            status: '',
            paid_total: 0,
            accountProducts: [],

            //Products
            isLoading: false,
            products: [],
            searchName: '',

            //Dishes
            isLoadingD: false,
            dishes: [],
            searchDish: '',

            productsPagination: {
                total: 0,
                current_page: 0,
                per_page: 0,
                last_page: 0,
                from: 0,
                to: 0,
            },

            dishesPagination: {
                total: 0,
                current_page: 0,
                per_page: 0,
                last_page: 0,
                from: 0,
                to: 0,
            },

            offset: 3,

            edit: false,

            errors: [],
        },

        computed: {

            isActived: function (pagination)
            {
                return this.productsPagination.current_page;
            },

            isActivedDishes: function (pagination)
            {
                return this.dishesPagination.current_page;
            },

            pagesNumber: function ()
            {
                if (!this.productsPagination.to)
                {
                    return [];
                }

                let from = this.productsPagination.current_page - this.offset;

                if (from < 1){
                    from = 1;
                }

                let to = from + (this.offset * 2);

                if (to >= this.productsPagination.last_page)
                {
                    to = this.productsPagination.last_page;
                }

                let pagesArray = [];

                while (from <= to)
                {
                    pagesArray.push(from);
                    from ++;
                }

                return pagesArray;
            },

            pagesNumberDishes: function ()
            {
                if (!this.dishesPagination.to)
                {
                    return [];
                }

                let from = this.dishesPagination.current_page - this.offset;

                if (from < 1){
                    from = 1;
                }

                let to = from + (this.offset * 2);

                if (to >= this.dishesPagination.last_page)
                {
                    to = this.dishesPagination.last_page;
                }

                let pagesArray = [];

                while (from <= to)
                {
                    pagesArray.push(from);
                    from ++;
                }

                return pagesArray;
            },
        },

        watch: {

            accountProducts: function()
            {
                this.paid_total = 0;

                this.accountProducts.forEach(product => {
                    console.log(product.total);
                    this.paid_total += parseInt(product.total);
                    console.log(this.paid_total);
                });
            },
        },

        methods: {

            getInvoices: function () {
                var url = '/invoices/data/1';

                axios.get(url)
                    .then(response => {
                        this.invoices = response.data;
                        this.setDataTable();
                    });
            },

            openModal: function (type) {
                $('#createInvoice').modal('show');
            },

            closeModal: function () {
                this.edit = false;
                this.searchName = '';
                this.cleanFields();
                $('#createInvoice').modal('hide');
            },

            getProducts: async function (page) {
                try {

                    this.isLoading = true;

                    let url = '/products/list';

                    if (!page)
                    {
                        page = 1;
                    }

                    await axios.post(
                        url,
                        {
                            page: page,
                            name: this.searchName,
                            pages: 3,
                        })
                        .then(response => {
                            this.products = response.data.products.data;
                            this.productsPagination = response.data.paginate
                        });
                }
                finally {
                    this.isLoading = false;
                }
            },

            getDishes: async function (page) {
                try {

                    this.isLoadingD = true;

                    let url = '/dishes/list';

                    if (!page)
                    {
                        page = 1;
                    }

                    await axios.post(
                        url,
                        {
                            page: page,
                            name: this.searchName,
                            pages: 3,
                        })
                        .then(response => {
                            this.dishes = response.data.dishes.data;
                            this.dishesPagination = response.data.paginate
                        });
                }
                finally {
                    this.isLoadingD = false;
                }
            },

            changePage: function (page) {
                this.productsPagination.current_page = page;
                this.getProducts(page);
            },

            changePageD: function (page) {
                this.dishesPagination.current_page = page;
                this.getDishes(page);
            },

            addAccount: function (product) {
                this.accountProducts.push(product);

                toastr.options = {
                    "progressBar": true,
                    "positionClass": "toast-top-center",
                }

                toastr.success('Se agrego a la cuenta','¡Exito!');
            },

            deleteAccount: function (product) {

                for (let i = 0; i < this.accountProducts.length ; i++) {
                    if (this.accountProducts[i].name == product.name)
                    {
                        this.accountProducts.splice(i, 1);
                    }
                }

            },

            createInvoice: function () {
                var url = 'invoices';

                axios.post(
                    url,
                    {
                        //Client Data
                        customer_id: this.customer_id,
                        first_name: this.first_name,
                        last_name: this.last_name,
                        identification: this.identification,
                        phone: this.phone,

                        //Invoice Data
                        status: this.status,
                        paid_value: this.paid_total,

                        //Products
                        products: this.accountProducts,
                    }
                )
                    .then(response => {
                        $("#sellTable").DataTable().destroy();
                        this.getInvoices();
                        this.cleanFields();
                        toastr.success('Se creo la cuenta con exito');
                    })
                    .catch(error => {
                        this.errors = error.response.data;
                    });
            },

            editInvoice: function (invoice) {

                this.invoiceId = invoice.id;
                this.edit = true;
                this.searchCustomer = invoice.customer.person.identification;
                this.getCustomer();
                this.status = invoice.invoice_status_id;

                invoice.products.forEach(product => {

                    let p = '';

                    if (!product.dish_id)
                    {
                        p = {
                            id: product.product.id,
                            name: product.product.name,
                            price: product.product.price,
                            quantity: product.quantity,
                            total: product.price,
                        }
                    }
                    else
                    {
                        p = {
                            id: product.dish.id,
                            name: product.dish.name,
                            price: product.dish.price,
                            quantity: product.quantity,
                            total: product.price,
                        }
                    }

                    this.accountProducts.push(p);
                });
            },

            updateInvoice: function () {

                var url = '/invoices/' + this.invoiceId;

                axios.put(
                    url,
                    {
                        //Invoice Data
                        status: this.status,
                        paid_value: this.paid_total,

                        //Products
                        products: this.accountProducts,
                    }
                )
                    .then(response => {
                        $("#invoicesTable").DataTable().destroy();
                        this.getInvoices();
                        this.cleanFields();
                        toastr.success('Se actualizaron los datos de la cuenta');
                    })
                    .catch(error => {
                        this.errors = error.response.data;
                    });
            },

            deleteInvoice: function (id) {

                var url = '/invoices/' + id;

                Swal.fire({
                    title: '¿Esta seguro?',
                    text: "Esta acción no se puede revertir",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: 'black',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Si, eliminar registro',
                    cancelButtonText: 'No. conservarlo',
                }).then((result) => {
                    if (result.value) {
                        axios.delete(url)
                            .then(response => {
                                $("#sellTable").DataTable().destroy();
                                this.getInvoices();
                                toastr.success('Eliminado correctamente');
                            })
                            .catch(error => {
                                toastr.error('Fallo al eliminar');
                            });
                    }
                })
            },

            generateInvoice: function (id) {
                var url = '/invoice/pdf/' + id;

                axios.get(url)
                    .then(response => {
                        let win = window.open(response.data, '_blank');
                        win.focus();
                    });
            },

            getCustomer: function () {
                let url = '/customers/namesearch';

                axios.post(
                    url,
                    {
                        name: this.searchCustomer,
                    }
                )
                    .then(response => {

                        this.customer_id = (response.data[0]) ? response.data[0].customer_id : false;
                        this.first_name = (response.data[0]) ? response.data[0].first_name : '';
                        this.last_name = (response.data[0]) ? response.data[0].last_name : '';
                        this.identification = (response.data[0]) ? response.data[0].identification : '';
                        this.phone = (response.data[0]) ? response.data[0].phone : '';

                    })
            },

            setDataTable: function () {
                $(document).ready(function () {
                    $('#sellTable').DataTable({
                        "orderCellsTop": true,
                        "fixedHeader": true,
                        "language": {
                            "lengthMenu": "Mostrar _MENU_ registros",
                            "info": "Mostrando registros desde _START_ hasta _END_ de _TOTAL_ registros",
                            "infoFiltered": "(filtrado de _MAX_ entradas totales)",
                            "search": "Buscar:",
                            "zeroRecords": "No se encontraron registros coincidentes",
                            "paginate": {
                                "first": "Primero",
                                "last": "Ultimo",
                                "next": "Siguiente",
                                "previous": "Anterior"
                            },
                        },
                    });
                })
            },

            cleanFields: function () {

                this.searchCustomer = '';

                //Client Data
                this.customer_id = false;
                this.first_name = '';
                this.last_name = '';
                this.identification = '';
                this.phone = '';

                //Account
                this.status = '';
                this.paid_total = 0;
                this.accountProducts = [];

                //Products
                this.searchName = '';

            },

        },

        mounted() {
            this.getInvoices();
            this.getProducts();
            this.getDishes();
        },
    });
}
