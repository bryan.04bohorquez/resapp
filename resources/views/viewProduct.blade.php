<div class="modal fade" id="detailProduct" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">sd
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" @click="closeDetail" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body" style="font-size: 18px">

                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h3 style="text-align: center">@{{ product.id }}. @{{ product.name }}</h3>
                        </div>
                        <div class="col-md-12">
                            <a class="btn btn-primary btn-block" style="color: white; margin-top: 20px; margin-bottom: 20px">@{{ product.category.name }}</a>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <p><strong>Nombre:</strong><br>@{{ product.name }}</p>
                        </div>
                        <div class="col-md-6">
                            <p><strong>Marca:</strong><br>@{{ product.brand }}</p>
                        </div>
                        <div class="col-md-6">
                            <p><strong>Provedor:</strong><br>@{{ product.provider.name }}</p>
                        </div>
                        <div class="col-md-6">
                            <p><strong>Categoria:</strong><br>@{{ product.category.name }}</p>
                        </div>

                        <div class="col-md-12">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th scope="col">Precio Venta</th>
                                    <th scope="col">Precio Costo</th>
                                    <th scope="col">Stock</th>
                                </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>@{{ product.price }}</td>
                                        <td>@{{ product.cost }}</td>
                                        <td>@{{ product.stock }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
