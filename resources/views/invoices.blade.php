@extends('layouts.app')

@section('content')

    <div id="invoices" style="margin-left: 20px; margin-right: 20px">

        <div v-show="loading" class="row align-items-center" style="height: 600px">
            <div class="col-md-12" style="margin-top: 50px; margin-bottom: 50px;">
                <div class="row justify-content-center">
                    <div class="col-md-12 text-center">
                        <div class="spinner-grow" style="width: 3rem; height: 3rem; margin-bottom: 50px" role="status">
                            <span class="sr-only">Loading...</span>
                        </div>
                        <h3 style="text-align: center;">Cargando...</h3>
                    </div>
                </div>
            </div>
        </div>

        <div v-show="!loading" class="container-fluid table-responsive-lg">

            <div class="row justify-content-end" style="margin: 0px ;margin-bottom: 20px">
                <a @click="openModal('Create')" style="color: white; background-color: black; border-color: black; margin: 10px;" class="btn btn-lg btn-primary"><i class="fa fa-plus"></i>&nbsp;&nbsp;&nbsp;Añadir</a>
                <a @click="openReport" style="color: white; background-color: black; border-color: black; margin: 10px;" class="btn btn-lg btn-primary"><i class="fa fa-file-excel"></i>&nbsp;&nbsp;&nbsp;Reporte</a>
            </div>

            <table id="invoicesTable" class="table table-striped table-bordered" style="font-size: 17px">
                <thead class="thead-dark" style="background-color: black">
                <tr>
                    <th style="background-color: black">ID</th>
                    <th style="background-color: black"><i class="fas fa-user"></i>&nbsp;&nbsp;Cliente</th>
                    <th style="background-color: black"> <i class="fas fa-coins"></i>&nbsp;&nbsp;Estado</th>
                    <th style="background-color: black"> <i class="fas fa-box"></i>&nbsp;&nbsp;Productos (Cantidad)</th>
                    <th style="background-color: black"> <i class="fas fa-dolar"></i>&nbsp;&nbsp;Valor</th>
                    <th style="background-color: black">&nbsp;&nbsp;Acciones</th>
                </tr>
                </thead>
                <tbody>
                <tr v-for="invoice in invoices">
                    <td>@{{ invoice.id }}</td>
                    <td>@{{ invoice.customer.person.first_name + " " + invoice.customer.person.last_name }} </td>
                    <td><h3><span style="width: 100%" class="badge" :class="[ invoice.status.name == 'pagada' ? 'badge-primary' : 'badge-success']">@{{ invoice.status.name }}</span></h3></td>
                    <td>
                        <ul>
                            <li v-for="p in invoice.products">
                                @{{ p.dish_id ? p.dish.name : p.product.name }}
                            </li>
                        </ul>
                    </td>
                    <td>@{{ invoice.paid_value }}</td>
                    <td>
                        <a @click="generateInvoice(invoice.id)" class="btn btn-primary" style="color: white; background-color: black; border-color: black;"><i class="fas fa-file-pdf fa-lg"></i></a>
                        <a @click="editInvoice(invoice)" class="btn btn-primary" style="color: white; background-color: black; border-color: black;"><i class="fas fa-user-edit fa-lg"></i></a>
                        <a @click="deleteInvoice(invoice.id)" class="btn btn-danger" style="color: white;"><i class="fas fa-trash fa-lg"></i></a>
                    </td>
                </tr>
                </tbody>
            </table>

        </div>

        @include('createInvoice')
        @include('sellReport')
    </div>

@endsection
