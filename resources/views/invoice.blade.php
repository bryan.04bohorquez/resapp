<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Factura</title>
    <style>
        .clearfix:after {
            content: "";
            display: table;
            clear: both;
        }

        a {
            color: #5D6975;
            text-decoration: underline;
        }

        body {
            position: relative;
            width: 18cm;
            height: 29.7cm;
            margin: 0 auto;
            color: #001028;
            background: #FFFFFF;
            font-family: Arial, sans-serif;
            font-size: 12px;
            font-family: Arial;
        }

        header {
            padding: 10px 0;
            margin-bottom: 30px;
        }

        #logo {
            text-align: center;
            margin-bottom: 10px;
        }

        #logo img {
            width: 90px;
        }

        h1 {
            border-top: 1px solid  #5D6975;
            border-bottom: 1px solid  #5D6975;
            color: #5D6975;
            font-size: 2.4em;
            line-height: 1.4em;
            font-weight: normal;
            text-align: center;
            margin: 0 0 20px 0;
            background: url(/images/dimension.png);
        }

        #project {
            width: 50%;
            float: left;
        }

        #project span {
            color: #5D6975;
            width: 80px;
            margin-right: 0px;
            display: inline-block;
            font-size: 0.8em;
        }

        #company {
            width: 50%;
            float: right;
            text-align: right;
        }

        #project div,
        #company div {
            white-space: nowrap;
        }

        table {
            width: 100%;
            border-collapse: collapse;
            border-spacing: 0;
            margin-bottom: 20px;
        }

        table tr:nth-child(2n-1) td {
            background: #F5F5F5;
        }

        table th,
        table td {
            text-align: center;
        }

        table th {
            padding: 5px 20px;
            color: #5D6975;
            border-bottom: 1px solid #C1CED9;
            white-space: nowrap;
            font-weight: normal;
        }

        table .service,
        table .desc {
            text-align: left;
        }

        table td {
            padding: 20px;
            text-align: right;
        }

        table td.service,
        table td.desc {
            vertical-align: top;
        }

        table td.unit,
        table td.qty,
        table td.total {
            font-size: 1.2em;
        }

        table td.grand {
            border-top: 1px solid #5D6975;;
        }

        #notices .notice {
            color: #5D6975;
            font-size: 1.2em;
        }

        footer {
            color: #5D6975;
            width: 100%;
            height: 30px;
            position: absolute;
            bottom: 0;
            border-top: 1px solid #C1CED9;
            padding: 8px 0;
            text-align: center;
        }

    </style>
</head>
<body>
<header class="clearfix">

    <h1>Cuenta No. {{ $invoice->id }}</h1>
    <div id="company" class="clearfix">
        <div>Company Name</div>
        <div>455 Foggy Heights,<br /> AZ 85004, US</div>
        <div>(602) 519-0450</div>
        <div><a href="mailto:company@example.com">company@example.com</a></div>
    </div>
    <div id="project">
        <div><span>CLIENTE:  </span> {{ $invoice->customer->person->first_name . " " . $invoice->customer->person->last_name }}</div>
        <div><span>IDENTIFICACIÓN: </span> {{ $invoice->customer->person->identification }}</div>
        <div><span>TELEFONO: </span> {{ $invoice->customer->person->phone }} </div>
        <div><span>FECHA: </span> {{ $invoice->updated_at }}</div>
    </div>
</header>
<main>
    <table>
        <thead>
        <tr>
            <th class="service">Producto</th>
            <th class="desc">Marca</th>
            <th>Precio (Unidad)</th>
            <th>CANT.</th>
            <th>TOTAL</th>
        </tr>
        </thead>
        <tbody>
        @foreach($invoice->products as $product)
            @if($product->product_id)
                <tr>
                    <td class="service">{{ $product->product->name }}</td>
                    <td class="desc">{{ $product->product->brand }}</td>
                    <td class="unit">{{ $product->product->price }}</td>
                    <td class="qty">{{ $product->quantity }}</td>
                    <td class="total">{{ $product->price }}</td>
                </tr>
            @else
                <tr>
                    <td class="service">{{ $product->dish->name }}</td>
                    <td class="desc"></td>
                    <td class="unit">{{ $product->dish->price }}</td>
                    <td class="qty">{{ $product->quantity }}</td>
                    <td class="total">{{ $product->price }}</td>
                </tr>
            @endif
        @endforeach
        <tr>
            <td colspan="4" class="grand total">TOTAL</td>
            <td class="grand total">{{ $invoice->paid_value }}</td>
        </tr>
        </tbody>
    </table>
{{--    <div id="notices">--}}
{{--        <div>NOTICE:</div>--}}
{{--        <div class="notice">A finance charge of 1.5% will be made on unpaid balances after 30 days.</div>--}}
{{--    </div>--}}
</main>
<footer>
    La factura se creó en una computadora y es válida sin la firma y el sello.
</footer>
</body>
</html>
