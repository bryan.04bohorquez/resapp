@extends('layouts.app')

@section('content')
<div id="dashboard" style="margin-left: 20px; margin-right: 20px">
    <div class="container-fluid">

        <div class="row" style="margin-bottom: 25px">

            <div @click="sell" class="col-md-3" style="margin-bottom: 10px">
                <div  class="card hvr-reveal hvr-grow-shadow hvr-shadow" style="border-radius: 45px;">
                    <img class="card-img-top" src="/images/icons/cash.svg" style="width: 150px; margin: auto; margin-bottom: 20px; margin-top: 20px; display:block;">
                    <div class="card-body">
                        <h1 class="card-title" style="text-align: center; font-family: myriad;">VENDER</h1>
                        <p class="card-text" style="text-align: center; font-family: myriad-light; font-size: 20px">Gestione las ventas.</p>
                    </div>
                </div>
            </div>

            <div @click="providers" class="col-md-3" style="margin-bottom: 10px">
                <div class="card hvr-reveal hvr-grow-shadow" style="border-radius: 45px;">
                    <img class="card-img-top" src="/images/icons/proveedores.svg" style="width: 135px; margin: auto; margin-bottom: 20px; margin-top: 20px; display:block;">
                    <div class="card-body">
                        <h1 class="card-title" style="text-align: center; font-family: myriad;">PROVEEDORES</h1>
                        <p class="card-text" style="text-align: center; font-family: myriad-light; font-size: 20px">Gestione sus proveedores.</p>
                    </div>
                </div>
            </div>

            <div @click="products" class="col-md-3" style="margin-bottom: 10px">
                <div class="card hvr-reveal hvr-grow-shadow" style="border-radius: 45px;">
                    <img class="card-img-top" src="/images/icons/productos.svg" style="width: 150px; margin: auto; margin-bottom: 20px; margin-top: 20px; display:block;">
                    <div class="card-body">
                        <h1 class="card-title" style="text-align: center; font-family: myriad;">PRODUCTOS</h1>
                        <p class="card-text" style="text-align: center; font-family: myriad-light; font-size: 20px">Gestione sus productos.</p>
                    </div>
                </div>
            </div>

            <div @click="users" class="col-md-3" style="margin-bottom: 10px">
                <div class="card hvr-reveal hvr-grow-shadow" style="border-radius: 45px;">
                    <img class="card-img-top" src="/images/icons/usuarios.svg" style="width: 150px; margin: auto; margin-bottom: 20px; margin-top: 20px; display:block;">
                    <div class="card-body">
                        <h1 class="card-title" style="text-align: center; font-family: myriad;">USUARIOS</h1>
                        <p class="card-text" style="text-align: center; font-family: myriad-light; font-size: 20px">Gestione los usuarios.</p>
                    </div>
                </div>
            </div>

        </div>

        <div class="row" style="margin-bottom: 25px">

            <div @click="dishes" class="col-md-3" style="margin-bottom:10px">
                <div class="card hvr-reveal hvr-grow-shadow hvr-grow-shadow" style="border-radius: 45px;">
                    <img class="card-img-top" src="/images/icons/food.svg" style="width: 150px; margin: auto; margin-bottom: 20px; margin-top: 20px; display:block;">
                    <div class="card-body">
                        <h1 class="card-title" style="text-align: center; font-family: myriad;">PLATOS</h1>
                        <p class="card-text" style="text-align: center; font-family: myriad-light; font-size: 20px">Gestione sus platillos.</p>
                    </div>
                </div>
            </div>

            <div  @click="customers" class="col-md-3" style="margin-bottom:10px">
                <div class="card hvr-reveal hvr-grow-shadow" style="border-radius: 45px;">
                    <img class="card-img-top" src="/images/icons/clientes.svg" style="width: 150px; margin: auto; margin-bottom: 20px; margin-top: 20px; display:block;">
                    <div class="card-body">
                        <h1 class="card-title" style="text-align: center; font-family: myriad;">CLIENTES</h1>
                        <p class="card-text" style="text-align: center; font-family: myriad-light; font-size: 20px">Gestione sus clientes.</p>
                    </div>
                </div>
            </div>

            <div @click="invoices" class="col-md-3" style="margin-bottom:10px">
                <div class="card hvr-reveal hvr-grow-shadow" style="border-radius: 45px;">
                    <img class="card-img-top" src="/images/icons/cuentas.svg" style="width: 150px; margin: auto; margin-bottom: 20px; margin-top: 20px; display:block;">
                    <div class="card-body">
                        <h1 class="card-title" style="text-align: center; font-family: myriad;">CUENTAS</h1>
                        <p class="card-text" style="text-align: center; font-family: myriad-light; font-size: 20px">Genere cuentas de cobro.</p>
                    </div>
                </div>
            </div>

            <div @click="categories" class="col-md-3" style="margin-bottom:10px">
                <div class="card hvr-reveal hvr-grow-shadow" style="border-radius: 45px;">
                    <img class="card-img-top" src="/images/icons/shopping.svg" style="width: 150px; margin: auto; margin-bottom: 20px; margin-top: 20px; display:block;">
                    <div class="card-body">
                        <h1 class="card-title" style="text-align: center; font-family: myriad;">CATEGORIAS</h1>
                        <p class="card-text" style="text-align: center; font-family: myriad-light; font-size: 20px">Gestione las categorias de productos.</p>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
@endsection
