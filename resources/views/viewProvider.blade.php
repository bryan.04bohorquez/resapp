<div class="modal fade" id="detailProvider" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" @click="closeDetail" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body" style="font-size: 18px">

                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-md-12">
                            <h3 style="text-align: center; margin-bottom: 20px">@{{ provider.id }}. @{{ provider.name }}</h3>
                        </div>

                        <div style="margin-right: 20px" class="col-md-6 card">

                            <h3 style="margin-top: 10px">Datos Proveedor: </h3>
                            <hr>

                            <div class="row">
                                <div class="col-md-6">
                                    <p><strong>Nombre:</strong><br>@{{ provider.name }}</p>
                                </div>
                                <div v-if="type == 'enterprice'" class="col-md-6">
                                    <p><strong>Nit:</strong><br>@{{ provider.nit }}</p>
                                </div>
                                <div v-if="type == 'enterprice'" class="col-md-12">
                                    <p><strong>Razon Social:</strong><br>@{{ provider.business_name }}</p>
                                </div>
                                <div class="col-md-6">
                                    <p><strong>Dirección:</strong><br>@{{ provider.address }}</p>
                                </div>
                                <div class="col-md-6">
                                    <p><strong>Telefono:</strong><br>@{{ provider.phone }}</p>
                                </div>
                                <div class="col-md-6">
                                    <p><strong>Metodo de Pago:</strong><br>@{{ provider.payment_method }}</p>
                                </div>
                                <div class="col-md-6">
                                    <p><strong>Cuenta Bancaria:</strong><br>@{{ provider.bank_account }}</p>
                                </div>
                                <div class="col-md-12">
                                    <p><strong>IVA:</strong><br>@{{ provider.iva }}</p>
                                </div>
                                <div class="col-md-12">
                                    <p><strong>Frecuencia de Entrega:</strong><br>@{{ provider.delivery_frequency }}</p>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-5 card">

                            <h3 style="margin-top: 10px">Productos: </h3>
                            <hr>
                            <div class="row">
                                <div v-for="product in provider.products" class="col-md-6">
                                    <div style="margin-top: 10px" class="card">
                                        <div class="card-body">
                                            <h5 class="card-title"><strong> @{{ product.name }}</strong></h5>
                                            <p class="card-text">@{{ product.brand }}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>










            </div>

        </div>
    </div>
</div>
