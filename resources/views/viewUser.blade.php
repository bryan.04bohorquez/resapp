<div class="modal fade" id="detailUser" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" @click="closeDetail" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body" style="font-size: 18px">

                <h3 style="text-align: center">@{{ user.id }}. @{{ user.person.first_name + " " + user.person.last_name }}</h3>
                <a class="btn btn-primary btn-block" style="color: white; margin-top: 20px; margin-bottom: 20px">@{{ user.user_type.name }}</a>
                <p><strong>Nombre:</strong><br>@{{ user.person.first_name + " " + user.person.last_name }}</p>
                <p><strong>Nombre de Usuario:</strong><br>@{{ user.username }}</p>
                <p><strong>Identificación:</strong><br>@{{ user.person.identification }}</p>
                <p><strong>Correo Electronico:</strong><br>@{{ user.email }}</p>
                <p><strong>Creado:</strong><br>@{{ user.created_at }}</p>
                <p><strong>Actualizado por ultima vez:</strong><br>@{{ user.updated_at }}</p>
            </div>

        </div>
    </div>
</div>
