<div class="modal fade" id="createProvider" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">@{{ modalTitle }}</h5>
                <button type="button" class="close" @click="closeModal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-md-6 card" style="margin-right: 10px">

                            <div class="card-body">

                                <h3 class="card-title"> @{{ crudTitle }} </h3>

                                <hr>
                                <br>

                                <div class="form-group row">
                                    <label for="name" class="col-md-4 col-form-label text-md-right">Nombre</label>

                                    <div class="col-md-6">
                                        <input  v-model="name" id="name" type="text" :class="errors.name ? 'is-invalid' : ''" class="form-control " name="name" autocomplete="name" autofocus>
                                        <div v-for="error in errors.name" v-show="errors.name" class="invalid-feedback">
                                            @{{ error }}
                                        </div>
                                    </div>
                                </div>

                                <div v-if="type == 'enterprice'" class="form-group row">
                                    <label for="business_name" class="col-md-4 col-form-label text-md-right">Razon Social</label>

                                    <div class="col-md-6">
                                        <input v-model="business_name" id="business_name" type="text" :class="errors.business_name ? 'is-invalid' : ''" class="form-control" name="business_name" required autocomplete="business_name" autofocus>
                                        <div v-for="error in errors.business_name" v-show="errors.business_name" class="invalid-feedback">
                                            @{{ error }}
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="address" class="col-md-4 col-form-label text-md-right">Dirección</label>

                                    <div class="col-md-6">
                                        <input v-model="address" id="address" type="text" :class="errors.business_name ? 'is-invalid' : ''" class="form-control" name="address" required autocomplete="address" autofocus>
                                        <div v-for="error in errors.business_name" v-show="errors.business_name" class="invalid-feedback">
                                            @{{ error }}
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="phone" class="col-md-4 col-form-label text-md-right">Telefono</label>
                                    <div class="col-md-6">
                                        <input v-model="phone" id="phone" type="number" :class="errors.phone ? 'is-invalid' : ''" class="form-control" name="phone" required autocomplete="phone" autofocus>
                                        <div v-for="error in errors.phone" v-show="errors.phone" class="invalid-feedback">
                                            @{{ error }}
                                        </div>
                                    </div>
                                </div>

                                <div v-if="type == 'enterprice'" class="form-group row">
                                    <label for="nit" class="col-md-4 col-form-label text-md-right">NIT</label>

                                    <div class="col-md-6">
                                        <input v-model="nit" id="nit" type="number" :class="errors.nit ? 'is-invalid' : ''" class="form-control" name="nit" required autocomplete="nit" autofocus>
                                        <div v-for="error in errors.nit" v-show="errors.nit" class="invalid-feedback">
                                            @{{ error }}
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="payment_method" class="col-md-4 col-form-label text-md-right">Metodo de pago</label>

                                    <div class="col-md-6">
                                        <input v-model="payment_method" id="payment_method" type="text" :class="errors.payment_method ? 'is-invalid' : ''" class="form-control" name="payment_method" required autocomplete="payment_method" autofocus>
                                        <div v-for="error in errors.payment_method" v-show="errors.payment_method" class="invalid-feedback">
                                            @{{ error }}
                                        </div>
                                    </div>
                                </div>


                                <div class="form-group row">
                                    <label for="bank_account" class="col-md-4 col-form-label text-md-right">Cuenta de banco</label>

                                    <div class="col-md-6">
                                        <input v-model="bank_account" id="bank_account" type="number" :class="errors.bank_account ? 'is-invalid' : ''" class="form-control" name="bank_account" required autocomplete="bank_account">
                                        <div v-for="error in errors.bank_account" v-show="errors.bank_account" class="invalid-feedback">
                                            @{{ error }}
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="iva" class="col-md-4 col-form-label text-md-right">IVA</label>

                                    <div class="col-md-6">
                                        <input v-model="iva" id="iva" type="number" :class="errors.iva ? 'is-invalid' : ''" class="form-control" name="iva" required autocomplete="iva">
                                        <div v-for="error in errors.iva" v-show="errors.iva" class="invalid-feedback">
                                            @{{ error }}
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="delivery_frequency" class="col-md-4 col-form-label text-md-right">Frecuencia de entrega</label>

                                    <div class="col-md-6">
                                        <input v-model="delivery_frequency" id="delivery_frequency" type="number"  :class="errors.delivery_frequency ? 'is-invalid' : ''" class="form-control" name="delivery_frequency" required autocomplete="delivery_frequency">
                                        <div v-for="error in errors.delivery_frequency" v-show="errors.delivery_frequency" class="invalid-feedback">
                                            @{{ error }}
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>
                        <div v-if="type == 'enterprice'" class="col-md-5 card" style="margin-left: 10px">

                            <div class="card-body">

                                <h3 class="card-title">Datos del Representante</h3>

                                <hr>
                                <br>

                                <div class="form-group row">
                                    <label for="p_first_name" class="col-md-4 col-form-label text-md-right">Nombre</label>

                                    <div class="col-md-6">
                                        <input v-model="p_first_name" id="p_first_name" type="text" class="form-control " name="p_first_name" autocomplete="p_first_name" autofocus>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="p_last_name" class="col-md-4 col-form-label text-md-right">Apellido</label>

                                    <div class="col-md-6">
                                        <input v-model="p_last_name" id="p_last_name" type="text" class="form-control" name="p_last_name" required autocomplete="p_last_name" autofocus>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="p_identification" class="col-md-4 col-form-label text-md-right">Identificación</label>

                                    <div class="col-md-6">
                                        <input v-model="p_identification" id="address" type="number" class="form-control" name="p_identification" required autocomplete="p_identification" autofocus>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="p_phone" class="col-md-4 col-form-label text-md-right">Telefono</label>
                                    <div class="col-md-6">
                                        <input v-model="p_phone" id="p_phone" type="number" class="form-control" name="p_phone" required autocomplete="p_phone" autofocus>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" @click="closeModal">Cancelar</button>
                <button @click="save" type="button" class="btn btn-success">Guardar</button>
            </div>
        </div>
    </div>
</div>
