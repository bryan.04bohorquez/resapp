@extends('layouts.app')

@section('content')
    <div id="sell" class="container">
        <div class="row">

            @include('sell.client')

            <div class="col-md-8">
                <div class="card">
                    <div class="card-body">
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Platos</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Productos</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Cuenta</a>
                            </li>
                        </ul>

                        <div class="tab-content" id="myTabContent">
                            @include('sell.products.dishes')
                            @include('sell.products.products')
                            @include('sell.products.account')
                        </div>

                    </div>
                </div>
            </div>

        </div>

        <br>

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h3>Cuentas Activas</h3>
                        <hr>

                        <table id="sellTable" class="table" style="font-size: 17px">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th><i class="fas fa-user"></i>&nbsp;&nbsp;Cliente</th>
                                <th> <i class="fas fa-coins"></i>&nbsp;&nbsp;Estado</th>
                                <th> <i class="fas fa-box"></i>&nbsp;&nbsp;Productos (Cantidad)</th>
                                <th> <i class="fas fa-dolar"></i>&nbsp;&nbsp;Valor</th>
                                <th>&nbsp;&nbsp;Acciones</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr v-for="invoice in invoices">
                                <td>@{{ invoice.id }}</td>
                                <td>@{{ invoice.customer.person.first_name + " " + invoice.customer.person.last_name }} </td>
                                <td><h3><span style="width: 100%" class="badge" :class="[ invoice.status.name == 'pagada' ? 'badge-primary' : 'badge-success']">@{{ invoice.status.name }}</span></h3></td>
                                <td>
                                    <ul>
                                        <li v-for="product in invoice.products">
                                            @{{ !product.dish_id ? product.product.name + "  ( " + product.quantity + " )" : product.dish.name + "  ( " + product.quantity + " )"}}
                                        </li>
                                    </ul>
                                </td>
                                <td>@{{ invoice.paid_value }}</td>
                                <td>
                                    <a href="#sell" @click="editInvoice(invoice)" class="btn btn-primary" style="color: white; background-color: black; border-color: black;"><i class="fas fa-user-edit fa-lg"></i></a>
                                    <a @click="deleteInvoice(invoice.id)" class="btn btn-danger" style="color: white;"><i class="fas fa-trash fa-lg"></i></a>
                                </td>
                            </tr>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
