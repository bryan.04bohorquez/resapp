@extends('layouts.app')

@section('content')

<div id="products" style="margin-left: 20px; margin-right: 20px">

    <div class="container-fluid table-responsive-lg">

        <div v-show="isloading" class="row align-items-center" style="height: 600px">
            <div class="col-md-12" style="margin-top: 50px; margin-bottom: 50px;">
                <div class="row justify-content-center">
                    <div class="col-md-12 text-center">
                        <div class="spinner-grow" style="width: 3rem; height: 3rem; margin-bottom: 50px" role="status">
                            <span class="sr-only">Loading...</span>
                        </div>
                        <h3 style="text-align: center;">Cargando...</h3>
                    </div>
                </div>
            </div>
        </div>


        <div v-show="!isloading" class="row justify-content-end" style="margin: 0px ;margin-bottom: 20px">
            <a @click="exportExcel" style="color: white; background-color: black; border-color: black; margin: 10px;" class="btn btn-lg btn-primary"><i class="fa fa-file-excel"></i>&nbsp;&nbsp;&nbsp;Exportar</a>
            <a @click="openModal('Create')" style="color: white; background-color: black; border-color: black; margin: 10px;" class="btn btn-lg btn-primary"><i class="fa fa-plus"></i>&nbsp;&nbsp;&nbsp;Añadir</a>
        </div>

        <table id="productsTable" class="table table-striped table-bordered" style="font-size: 17px">
            <thead class="thead-dark" style="background-color: black">
                <tr>
                    <th style="background-color: black">ID</th>
                    <th style="background-color: black"><i class="fas fa-box-open"></i>&nbsp;&nbsp;Nombre</th>
                    <th style="background-color: black"> <i class="fas fa-coins"></i>&nbsp;&nbsp;Precio Venta</th>
                    <th style="background-color: black"> <i class="fas fa-box"></i>&nbsp;&nbsp;Stock</th>
                    <th style="background-color: black"> <i class="fas fa-tag"></i>&nbsp;&nbsp;Categoria</th>
                    <th style="background-color: black">&nbsp;&nbsp;Acciones</th>
                </tr>
            </thead>
            <tbody>
                <tr v-for="product in products">
                    <td>@{{ product.id }}</td>
                    <td>@{{ product.name + "  ("+product.brand+")" }} </td>
                    <td>@{{ product.price }}</td>
                    <td>@{{ product.stock }}</td>
                    <td>@{{ product.category.name }}</td>
                    <td>
                        <a @click="detailModal(product)" class="btn btn-primary" style="color: white; background-color: black; border-color: black;"><i class="fas fa-search-plus fa-lg"></i></a>
                        <a @click="editModal(product)" class="btn btn-primary" style="color: white; background-color: black; border-color: black;"><i class="fas fa-user-edit fa-lg"></i></a>
                        <a @click="deleteProduct(product.id)" class="btn btn-danger" style="color: white;"><i class="fas fa-trash fa-lg"></i></a>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>

    @include('createProduct')

    @include('viewProduct')

</div>

@endsection
