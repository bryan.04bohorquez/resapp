<div class="modal fade" id="sellReport" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Generar Reporte Ventas</h5>
                <button type="button" class="close" @click="closeReport" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group row">
                    <label for="since" class="col-md-4 col-form-label text-md-right">Desde: </label>
                    <div class="col-md-6">
                        <input v-model="start_date" class="form-control" type="text" id="startDate">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="endDate" class="col-md-4 col-form-label text-md-right">Hasta: </label>

                    <div class="col-md-6">
                        <input v-model="end_date" class="form-control" type="text" id="endDate">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button @click="closeReport" type="button" class="btn btn-danger">Cancelar</button>
                <button @click="genReport" type="button" class="btn btn-success">Generar</button>
            </div>
        </div>
    </div>
</div>
