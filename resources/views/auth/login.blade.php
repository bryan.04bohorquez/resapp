<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Iniciar Sesion</title>

    <link href="{{ asset('css/login.css') }}" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>

<body>

    <div class="outer">
        <div class="middle">
            <div class="inner">
                <div id="app" class="container align-self-center">

                    <div class="row justify-content-center">
                        <div class="col-md-6">
                            <div id="login" class="card">
                                <div class="card-body">
                                    <div class="row justify-content-center">
                                        <div style="margin-top: 30px; margin-bottom: 25px" class="col-md-3">
                                            <img src="/images/icons/user.svg" width="101">
                                        </div>
                                    </div>
                                    <div class="row justify-content-center">
                                        <div class="col-md-8">
                                            <h1 style="text-align: center; font-family: myriad; letter-spacing: 8px">BIENVENIDO</h1>
                                        </div>
                                    </div>
                                    <div class="row justify-content-center">
                                        <div class="col-md-8">
                                            <h5 style="text-align: center;font-family: myriad-light; font-size: 25px; color: #4d4d4d;font-weight: 700; letter-spacing: 5px">Ingresa tus datos</h1>
                                        </div>
                                    </div>
                                    <div class="row justify-content-center" style="margin-top: 30px">
                                        <div class="col-md-12">
                                            <form method="POST" action="{{ route('login') }}">
                                                @csrf
                                                <div class="form-group">
                                                    <label style="font-size: 20px; font-family: myriad-light" for="username">Usuario</label>
                                                    <input v-model="username" id="username" type="name" class="form-control @error('username') is-invalid @enderror" name="username" value="{{ old('username') }}" autocomplete="username" autofocus>

                                                    @error('username')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror


                                                </div>
                                                <div class="form-group">
                                                    <label style="font-size: 20px; font-family: myriad-light" for="exampleInputPassword1">Contraseña</label>
                                                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" autocomplete="current-password">

                                                    @error('password')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror

                                                </div>

                                                <div class="form-group row mb-0">
                                                    <div class="col-md-12">
                                                        <button style="margin-top: 30px; font-family: myriad" type="submit" class="btn btn-dark btn-block btn-lg">INGRESAR</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style="margin-top: 20px;" class="row justify-content-center">
                        <h6>
                            <a style="color: white; font-size: 25px" class="btn btn-link" href="{{ route('password.request') }}">
                                ¿Olvido su contraseña?
                            </a>
                        </h6>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

</body>

</html>
