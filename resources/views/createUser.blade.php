<div class="modal fade" id="createUser" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">@{{ modalTitle }}</h5>
                <button type="button" class="close" @click="closeModal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group row">
                    <label for="username" class="col-md-4 col-form-label text-md-right">Nombre de usuario</label>

                    <div class="col-md-6">
                        <input v-model="username" id="username" type="text" :class="errors.username ? 'is-invalid' : ''" class="form-control " name="username" autocomplete="username" autofocus>
                        <div v-for="error in errors.username" v-show="errors.username" class="invalid-feedback">
                            @{{ error }}
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="first_name" class="col-md-4 col-form-label text-md-right">Nombre</label>

                    <div class="col-md-6">
                        <input v-model="first_name" id="first_name" type="text" :class="errors.first_name ? 'is-invalid' : ''" class="form-control" name="first_name" required autocomplete="first_name" autofocus>
                        <div v-for="error in errors.first_name" v-show="errors.first_name" class="invalid-feedback">
                            @{{ error }}
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="first_name" class="col-md-4 col-form-label text-md-right">Apellido</label>

                    <div class="col-md-6">
                        <input v-model="last_name" id="last_name" type="text" :class="errors.last_name ? 'is-invalid' : ''" class="form-control" name="last_name" required autocomplete="first_name" autofocus>
                        <div v-for="error in errors.last_name" v-show="errors.last_name" class="invalid-feedback">
                            @{{ error }}
                        </div>
                    </div>
                </div>


                <div class="form-group row">
                    <label for="identification" class="col-md-4 col-form-label text-md-right">Identificación</label>
                    <div class="col-md-6">
                        <input v-model="identification" id="identification" type="text" :class="errors.identification ? 'is-invalid' : ''" class="form-control" name="identification" required autocomplete="identification" autofocus>
                        <div v-for="error in errors.identification" v-show="errors.identification" class="invalid-feedback">
                            @{{ error }}
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="user_type" class="col-md-4 col-form-label text-md-right">Tipo de usuario</label>

                    <div class="col-md-6">
                        <select v-model="user_type" id="user_type" name="user_type" :class="errors.user_type ? 'is-invalid' : ''" class="form-control @error('user_type_id') is-invalid @enderror" required autocomplete="user_type_id" autofocus>
                            @foreach($user_types as $type)
                            <option value="{{$type->id}}">{{$type->name}}</option>
                            @endforeach
                        </select>
                        <div v-for="error in errors.user_type" v-show="errors.user_type" class="invalid-feedback">
                            @{{ error }}
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="phone" class="col-md-4 col-form-label text-md-right">Telefono</label>

                    <div class="col-md-6">
                        <input v-model="phone" id="phone" type="text" :class="errors.phone ? 'is-invalid' : ''" class="form-control" name="phone" required autocomplete="phone" autofocus>
                        <div v-for="error in errors.phone" v-show="errors.phone" class="invalid-feedback">
                            @{{ error }}
                        </div>
                    </div>
                </div>


                <div class="form-group row">
                    <label for="email" class="col-md-4 col-form-label text-md-right">Correo electronico</label>

                    <div class="col-md-6">
                        <input v-model="email" id="email" type="email" :class="errors.email ? 'is-invalid' : ''" class="form-control" name="email" required autocomplete="email">
                        <div v-for="error in errors.email" v-show="errors.email" class="invalid-feedback">
                            @{{ error }}
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="password" class="col-md-4 col-form-label text-md-right">@{{ labelContraseña }}</label>

                    <div class="col-md-6">
                        <input v-model="password" id="password" type="password" :class="errors.password ? 'is-invalid' : ''"  class="form-control" name="password" required autocomplete="new-password">
                        <div v-for="error in errors.password" v-show="errors.password" class="invalid-feedback">
                            @{{ error }}
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="password-confirm" class="col-md-4 col-form-label text-md-right">Confirmar contraseña</label>

                    <div class="col-md-6">
                        <input v-model="password_confirm" id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" @click="closeModal">Cancelar</button>
                <button @click="save" type="button" class="btn btn-success">Guardar</button>
            </div>
        </div>
    </div>
</div>
