<div class="modal fade" id="createDish" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Añadir Platos</h5>
                <button type="button" class="close" @click="closeModal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-md-5 card" style="margin-right: 10px">

                            <div class="card-body">

                                <h3 class="card-title">Datos Plato: </h3>
                                <hr>

                                <div class="form-group row">
                                    <label for="name" class="col-md-12 col-form-label text-md-left">Nombre</label>

                                    <div class="col-md-12">
                                        <input v-model="name" id="name" type="text" class="form-control " name="name" autocomplete="name" autofocus>
                                    </div>
                                </div>

                                <div class="form-group row">

                                    <label for="description" class="col-md-12 col-form-label text-md-left">Descripción</label>

                                    <div class="col-md-12">
                                        <textarea v-model="description" class="form-control" id="description" rows="3"></textarea>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-md-6">
                                        <label for="price" class="text-md-left">Precio</label>
                                        <input v-model="price" id="price" type="number" class="form-control" name="price" required autocomplete="price" autofocus>
                                    </div>

                                    <div class="col-md-6">
                                        <label for="cost" class="text-md-left">Costo</label>
                                        <input disabled v-model="cost" id="cost" type="text" class="form-control" name="cost" required autocomplete="cost" autofocus>
                                    </div>

                                </div>

                            </div>

                        </div>
                        <div class="col-md-6 card">

                            <div class="card-body">
                                <h3 class="card-title">Ingredientes: </h3>
                                <hr>
                                <div class="form-group row">
                                    <label for="searchName" class="col-md-2 col-form-label text-md-right">Buscar:</label>
                                    <div class="col-md-10">
                                        <input @change="getProducts" v-model="searchName" class="form-control" name="searchName" required autocomplete="searchName" autofocus>
                                    </div>
                                </div>

                                <div class="row">
                                    <div v-if="isLoading" class="col-md-12" style="margin-top: 50px; margin-bottom: 50px;">
                                        <div class="row justify-content-center">
                                            <div class="col-md-12 text-center">
                                                <div class="spinner-border" style="width: 3rem; height: 3rem; margin-bottom: 50px" role="status">
                                                    <span class="sr-only">Loading...</span>
                                                </div>
                                                <h3 style="text-align: center;">Cargando...</h3>
                                            </div>
                                        </div>
                                    </div>
                                    <div v-else v-for="ingredient in ingredients" class="col-md-6">
                                        <div style="margin-top: 10px" class="card">
                                            <div class="card-body">
                                                <h5 class="card-title"><strong> @{{ ingredient.name }}</strong></h5>
                                                <div>
                                                    <hr>
                                                    <div class="form-group row">
                                                        <label for="quantity" class="col-md-6 col-form-label text-md-right">Cantidad</label>
                                                        <div class="col-md-6">
                                                            <input v-model="ingredient.quantity" value="1" min="1" :max="ingredient.stock" id="quantity" type="number" class="form-control" name="quantity" required autocomplete="quantity" autofocus>
                                                        </div>
                                                    </div>
                                                    <a v-if="ingredient.quantity >= 1" @click="addIngredient(ingredient)" class="btn btn-primary btn-block" style="background-color: black; color: white; border-color: black;">Añadir</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <hr>

                                <div class="row">
                                    <div class="col-md-12">
                                        <nav aria-label="Page navigation example">
                                            <ul class="pagination">
                                                <li v-if="ingredientsPagination.current_page > 1" class="page-item"><a @click.prevent="changePage(ingredientsPagination.current_page - 1)" class="page-link" href="#">Anterior</a></li>

                                                <li :class="[ page == isActived ? 'active' : '']" v-for="page in pagesNumber" class="page-item"><a @click.prevent="changePage(page)" class="page-link" href="#">@{{ page }}</a></li>

                                                <li v-if="ingredientsPagination.current_page < ingredientsPagination.last_page" class="page-item"><a @click.prevent="changePage(ingredientsPagination.current_page + 1)"  class="page-link" href="#">Siguiente</a></li>
                                            </ul>
                                        </nav>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="row justify-content-center">
                        <div class="col-md-12">
                            <div class="card" style="margin: 23px">

                                <div style="margin:20px;">
                                    <h3 class="cart-title"> Ingredientes</h3>

                                    <hr>

                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">Nombre</th>
                                            <th scope="col">Precio (Unidad)</th>
                                            <th scope="col">Cantidad</th>
                                            <th scope="col">Total</th>
                                            <th scope="col"></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr v-for="ingredient in dishIngredients">
                                            <td>@{{ ingredient.id }}</td>
                                            <td>@{{ ingredient.name }}</td>
                                            <td>@{{ ingredient.price }}</td>
                                            <td>@{{ ingredient.quantity }}</td>
                                            <td>@{{ ingredient.total }}</td>
                                            <td>
                                                <a @click="deleteIngredient(ingredient)" class="btn btn-block" style="background-color: black; color: white;"> Quitar </a>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="modal-footer">
                <button @click="closeModal" type="button" class="btn btn-danger">Cancelar</button>
                <button @click="createDish"  v-if="!edit" type="button" class="btn btn-success">Guardar</button>
                <button v-else @click="updateDish" type="button" class="btn btn-success">Guardar</button>
            </div>
        </div>
    </div>
</div>
