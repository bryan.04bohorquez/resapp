<div class="modal fade" id="createCategory" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">@{{ modalTitle }}</h5>
                <button type="button" class="close" @click="closeModal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group row">
                    <label for="name" class="col-md-1 col-form-label text-md-right">Nombre</label>

                    <div class="col-md-12">
                        <input v-model="name" id="name" type="text" :class="errors.name ? 'is-invalid' : ''" class="form-control" name="name" autocomplete="name" autofocus>
                        <div v-for="error in errors.name" v-show="errors.name" class="invalid-feedback">
                            @{{ error }}
                        </div>
                    </div>

                </div>

                <div class="form-group">
                    <label for="description">Descripción</label>
                    <textarea v-model="description"  :class="errors.description ? 'is-invalid' : ''" class="form-control" id="description" rows="5"></textarea>
                    <div v-for="error in errors.description" v-show="errors.description" class="invalid-feedback">
                        @{{ error }}
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" @click="closeModal">Cancelar</button>
                <button @click="save" type="button" class="btn btn-success">Guardar</button>
            </div>
        </div>
    </div>
</div>
