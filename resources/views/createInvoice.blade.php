<div class="modal fade" id="createInvoice" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Añadir Cuenta</h5>
                <button type="button" class="close" @click="closeModal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-md-5 card" style="margin-right: 10px">

                            <div class="card-body">

                                <h3 class="card-title"> Datos Cliente </h3>
                                <hr>
                                <div class="form-group row">
                                    <label for="searchName" class="col-md-3 col-form-label text-md-right">Buscar:</label>
                                    <div class="col-md-9">
                                        <input :disabled="edit" @change="getCustomer" v-model="searchCustomer" class="form-control" name="searchCustomer" required autocomplete="searchCustomer" autofocus>
                                    </div>
                                </div>
                                <hr>

                                <div class="form-group row">
                                    <label for="name" class="col-md-4 col-form-label text-md-right">Nombre</label>

                                    <div class="col-md-6">
                                        <input :disabled="customer_id" v-model="first_name" id="name" type="text" class="form-control " name="name" autocomplete="name" autofocus>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="name" class="col-md-4 col-form-label text-md-right">Apellido</label>

                                    <div class="col-md-6">
                                        <input :disabled="customer_id" v-model="last_name" id="name" type="text" class="form-control " name="name" autocomplete="name" autofocus>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="identification" class="col-md-4 col-form-label text-md-right">Identifición</label>

                                    <div class="col-md-6">
                                        <input :disabled="customer_id" v-model="identification" id="identification" type="number" class="form-control" name="identification" required autocomplete="identification" autofocus>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="phone" class="col-md-4 col-form-label text-md-right">Telefono</label>
                                    <div class="col-md-6">
                                        <input :disabled="customer_id" v-model="phone" id="phone" type="text" class="form-control" name="phone" required autocomplete="phone" autofocus>
                                    </div>
                                </div>

                                <hr>

                                <div class="form-group row">
                                    <label for="status" class="col-md-4 col-form-label text-md-right">Estado</label>

                                    <div class="col-md-6">
                                        <select v-model="status" id="status" name="status" class="form-control col" required autocomplete="status" autofocus>
                                            @foreach($status as $state)
                                                <option value="{{$state->id}}">{{$state->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="paid_total" class="col-md-4 col-form-label text-md-right">Total</label>
                                    <div class="col-md-6">
                                        <input v-model="paid_total" disabled id="paid_total" type="text" class="form-control" name="paid_total" required autocomplete="paid_total" autofocus>
                                    </div>
                                </div>

                            </div>

                        </div>

                        <div class="col-md-6 card">
                            <div class="card-body">

                                <nav>
                                    <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                        <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Productos</a>
                                        <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Platos</a>
                                    </div
                                </nav>

                                {{--Productos--}}
                                <div class="tab-content" id="nav-tabContent">
                                    <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                                        <br>
                                        <div class="form-group row">
                                            <label for="searchName" class="col-md-2 col-form-label text-md-right">Buscar:</label>
                                            <div class="col-md-10">
                                                <input @change="getProducts" v-model="searchName" class="form-control" name="searchName" required autocomplete="searchName" autofocus>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div v-if="isLoading" class="col-md-12" style="margin-top: 50px; margin-bottom: 50px;">
                                                <div class="row justify-content-center">
                                                    <div class="col-md-12 text-center">
                                                        <div class="spinner-border" style="width: 3rem; height: 3rem; margin-bottom: 50px" role="status">
                                                            <span class="sr-only">Loading...</span>
                                                        </div>
                                                        <h3 style="text-align: center;">Cargando...</h3>
                                                    </div>
                                                </div>
                                            </div>
                                            <div v-else v-for="product in products" class="col-md-6">
                                                <div style="margin-top: 10px" class="card">
                                                    <div class="card-body">
                                                        <h5 class="card-title"><strong> @{{ product.name }}</strong></h5>
                                                        <p class="card-text">Precio Unidad:  @{{ product.price }}</p>
                                                        <div>
                                                            <hr>
                                                            <div class="form-group row">
                                                                <label for="quantity" class="col-md-6 col-form-label text-md-right">Cantidad</label>
                                                                <div class="col-md-6">
                                                                    <input v-model="product.quantity" value="1" min="1" :max="product.stock" id="quantity" type="number" class="form-control" name="quantity" required autocomplete="quantity" autofocus>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                            <h5 class="card-title" style="text-align: right">Total: @{{ product.total = product.quantity * product.price }}</h5>
                                                            <hr>
                                                            <a v-if="product.quantity >= 1" @click="addAccount(product)" class="btn btn-primary btn-block" style="background-color: black; color: white; border-color: black;">Añadir a cuenta</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <hr>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <nav aria-label="Page navigation example">
                                                    <ul class="pagination">
                                                        <li v-if="productsPagination.current_page > 1" class="page-item"><a @click.prevent="changePage(productsPagination.current_page - 1)" class="page-link" href="#">Anterior</a></li>

                                                        <li :class="[ page == isActived ? 'active' : '']" v-for="page in pagesNumber" class="page-item"><a @click.prevent="changePage(page)" class="page-link" href="#">@{{ page }}</a></li>

                                                        <li v-if="productsPagination.current_page < productsPagination.last_page" class="page-item"><a @click.prevent="changePage(productsPagination.current_page + 1)"  class="page-link" href="#">Siguiente</a></li>
                                                    </ul>
                                                </nav>
                                            </div>
                                        </div>
                                    </div>

                                    {{--Platos--}}
                                    <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                                        <br>
                                        <div class="form-group row">
                                            <label for="searchName" class="col-md-2 col-form-label text-md-right">Buscar:</label>
                                            <div class="col-md-10">
                                                <input @change="getDishes" v-model="searchName" class="form-control" name="searchName" required autocomplete="searchName" autofocus>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div v-if="isLoading" class="col-md-12" style="margin-top: 50px; margin-bottom: 50px;">
                                                <div class="row justify-content-center">
                                                    <div class="col-md-12 text-center">
                                                        <div class="spinner-border" style="width: 3rem; height: 3rem; margin-bottom: 50px" role="status">
                                                            <span class="sr-only">Loading...</span>
                                                        </div>
                                                        <h3 style="text-align: center;">Cargando...</h3>
                                                    </div>
                                                </div>
                                            </div>
                                            <div v-else v-for="dish in dishes" class="col-md-6">
                                                <div style="margin-top: 10px" class="card">
                                                    <div class="card-body">
                                                        <h5 class="card-title"><strong> @{{ dish.name }}</strong></h5>
                                                        <p class="card-text">Precio Unidad:  @{{ dish.price }}</p>
                                                        <div>
                                                            <hr>
                                                            <div class="form-group row">
                                                                <label for="quantity" class="col-md-6 col-form-label text-md-right">Cantidad</label>
                                                                <div class="col-md-6">
                                                                    <input v-model="dish.quantity" value="1" min="1" type="number" class="form-control" name="quantity" required autocomplete="quantity" autofocus>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                            <h5 class="card-title" style="text-align: right">Total: @{{ dish.total = dish.quantity * dish.price }}</h5>
                                                            <hr>
                                                            <a v-if="dish.quantity >= 1" @click="addAccount(dish)" class="btn btn-primary btn-block" style="background-color: black; color: white; border-color: black;">Añadir a cuenta</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <hr>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <nav aria-label="Page navigation example">
                                                    <ul class="pagination" v-if="dishes.length">
                                                        <li v-if="dishesPagination.current_page > 1" class="page-item"><a @click.prevent="changePageD(dishesPagination.current_page - 1)" class="page-link" href="#">Anterior</a></li>

                                                        <li :class="[ page == isActivedDishes ? 'active' : '']" v-for="page in pagesNumberDishes" class="page-item"><a @click.prevent="changePageD(page)" class="page-link" href="#">@{{ page }}</a></li>

                                                        <li v-if="dishesPagination.current_page < dishesPagination.last_page" class="page-item"><a @click.prevent="changePageD(dishesPagination.current_page + 1)"  class="page-link" href="#">Siguiente</a></li>
                                                    </ul>
                                                </nav>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>

                    </div>

                    <div class="row justify-content-center">
                        <div class="col-md-12">
                            <div class="card" style="margin: 23px">

                                <div style="margin:20px;">
                                    <h3 class="cart-title"> Productos</h3>

                                    <hr>

                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">Nombre</th>
                                            <th scope="col">Precio (Unidad)</th>
                                            <th scope="col">Cantidad</th>
                                            <th scope="col">Total</th>
                                            <th scope="col"></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            <tr v-for="product in accountProducts">
                                                <td>@{{ product.id }}</td>
                                                <td>@{{ product.name }}</td>
                                                <td>@{{ product.price }}</td>
                                                <td>@{{ product.quantity }}</td>
                                                <td>@{{ product.total }}</td>
                                                <td>
                                                    <a @click="deleteAccount(product)" class="btn btn-block" style="background-color: black; color: white;"> Quitar </a>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="modal-footer">
                <button @click="closeModal" type="button" class="btn btn-danger">Cancelar</button>
                <button v-if="!edit" @click="createInvoice" type="button" class="btn btn-success">Guardar</button>
                <button v-else @click="updateInvoice()" type="button" class="btn btn-success">Guardar</button>
            </div>
        </div>
    </div>
</div>
