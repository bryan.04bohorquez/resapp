@extends('layouts.app')

@section('content')

    <div id="providers" style="margin-left: 20px; margin-right: 20px">

        <div v-show="isloading" class="row align-items-center" style="height: 600px">
            <div class="col-md-12" style="margin-top: 50px; margin-bottom: 50px;">
                <div class="row justify-content-center">
                    <div class="col-md-12 text-center">
                        <div class="spinner-grow" style="width: 3rem; height: 3rem; margin-bottom: 50px" role="status">
                            <span class="sr-only">Loading...</span>
                        </div>
                        <h3 style="text-align: center;">Cargando...</h3>
                    </div>
                </div>
            </div>
        </div>

        <div v-show="!isloading" class="container-fluid table-responsive-lg">

            <div class="row justify-content-end" style="margin: 0px ;margin-bottom: 20px">
                <a @click="exportExcel" style="color: white; background-color: black; border-color: black; margin: 10px;" class="btn btn-lg btn-primary"><i class="fa fa-file-excel"></i>&nbsp;&nbsp;&nbsp;Exportar</a>
                <a @click="openModal('Create')" style="color: white; background-color: black; border-color: black; margin: 10px;" class="btn btn-lg btn-primary"><i class="fa fa-plus"></i>&nbsp;&nbsp;&nbsp;Añadir</a>
            </div>

            <table id="providersTable" class="table table-striped table-bordered" style="font-size: 17px">
                <thead class="thead-dark" style="background-color: black">
                <tr>
                    <th style="background-color: black">ID</th>
                    <th style="background-color: black"><i class="fas fa-build"></i>&nbsp;&nbsp;Nombre</th>
                    <th style="background-color: black"> <i class="fas fa-ubication"></i>Dirección</th>
                    <th style="background-color: black"> <i class="fas fa-phone"></i>&nbsp;&nbsp;Telefono</th>
                    <th style="background-color: black"> <i class="fas fa-clock"></i>&nbsp;&nbsp;Frecuencia</th>
                    <th style="background-color: black">&nbsp;&nbsp;Acciones</th>
                </tr>
                </thead>
                <tbody>
                <tr v-for="provider in providers">
                    <td>@{{ provider.id }}</td>
                    <td>@{{ provider.name }}</td>
                    <td>@{{ provider.address }}</td>
                    <td>@{{ provider.phone }}</td>
                    <td>@{{ provider.delivery_frequency }}</td>
                    <td>
                        <a @click="detailModal(provider)" class="btn btn-primary" style="color: white; background-color: black; border-color: black;"><i class="fas fa-box fa-lg"></i></a>
                        <a @click="editModal(provider)" class="btn btn-primary" style="color: white; background-color: black; border-color: black;"><i class="fas fa-user-edit fa-lg"></i></a>
                        <a @click="deleteProvider(provider.id)" class="btn btn-danger" style="color: white;"><i class="fas fa-trash fa-lg"></i></a>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>

        @include('createProvider')

        @include('viewProvider')

    </div>

@endsection
