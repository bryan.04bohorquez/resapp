<div class="modal fade" id="createProduct" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">@{{ modalTitle }}</h5>
                <button type="button" class="close" @click="closeModal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group row">
                    <label for="name" class="col-md-4 col-form-label text-md-right">Nombre</label>

                    <div class="col-md-6">
                        <input v-model="name" id="name" type="text" :class="errors.name ? 'is-invalid' : ''" class="form-control " name="name" autocomplete="name" autofocus>
                        <div v-for="error in errors.name" v-show="errors.name" class="invalid-feedback">
                            @{{ error }}
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="brand" class="col-md-4 col-form-label text-md-right">Marca</label>

                    <div class="col-md-6">
                        <input v-model="brand" id="brand" type="text" :class="errors.brand ? 'is-invalid' : ''" class="form-control " name="brand" autocomplete="brand" autofocus>
                        <div v-for="error in errors.brand" v-show="errors.brand" class="invalid-feedback">
                            @{{ error }}
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="price" class="col-md-4 col-form-label text-md-right">Precio Venta</label>

                    <div class="col-md-6">
                        <input v-model="price" id="price" type="number" :class="errors.price ? 'is-invalid' : ''" class="form-control" name="price" required autocomplete="price" autofocus>
                        <div v-for="error in errors.price" v-show="errors.price" class="invalid-feedback">
                            @{{ error }}
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="cost" class="col-md-4 col-form-label text-md-right">IVA Venta</label>

                    <div class="col-md-6">
                        <input v-model="iva_price" max="99" min="0" id="cost" type="number" :class="iva_price > 99 ? 'is-invalid' : ''" class="form-control" name="cost" required autocomplete="cost" autofocus>
                        <div v-show="iva_price > 99" class="invalid-feedback">
                            Valor Invalido
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="cost" class="col-md-4 col-form-label text-md-right">Precio Costo</label>

                    <div class="col-md-6">
                        <input v-model="cost" id="cost" type="number" :class="errors.price ? 'is-invalid' : ''" class="form-control" name="cost" required autocomplete="cost" autofocus>
                        <div v-for="error in errors.cost" v-show="errors.cost" class="invalid-feedback">
                            @{{ error }}
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="cost" class="col-md-4 col-form-label text-md-right">IVA Costo</label>

                    <div class="col-md-6">
                        <input v-model="iva_cost" max="99" min="0" id="cost" type="number" :class="iva_cost > 99 ? 'is-invalid' : ''" class="form-control" name="cost" required autocomplete="cost" autofocus>
                        <div v-show="iva_cost > 99" class="invalid-feedback">
                            Valor Invalido
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="stock" class="col-md-4 col-form-label text-md-right">Stock</label>

                    <div class="col-md-6">
                        <input v-model="stock" id="stock" type="number" :class="errors.stock ? 'is-invalid' : ''" class="form-control" name="stock" required autocomplete="stock" autofocus>
                        <div v-for="error in errors.stock" v-show="errors.stock" class="invalid-feedback">
                            @{{ error }}
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="type" class="col-md-4 col-form-label text-md-right">Tipo</label>

                    <div class="col-md-6">
                        <select v-model="type" id="type" name="type" :class="errors.type ? 'is-invalid' : ''" class="form-control" required autocomplete="type" autofocus>
                            @foreach($types as $type)
                                <option value="{{$type->id}}">{{$type->name}}</option>
                            @endforeach
                        </select>
                        <div v-for="error in errors.type" v-show="errors.type" class="invalid-feedback">
                            @{{ error }}
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="category" class="col-md-4 col-form-label text-md-right">Categoria</label>

                    <div class="col-md-6">
                        <select v-model="category" id="category" name="category" :class="errors.category ? 'is-invalid' : ''" class="form-control" required autocomplete="category" autofocus>
                            @foreach($categories as $category)
                            <option value="{{$category->id}}">{{$category->name}}</option>
                            @endforeach
                        </select>
                        <div v-for="error in errors.category" v-show="errors.category" class="invalid-feedback">
                            @{{ error }}
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="provider" class="col-md-4 col-form-label text-md-right">Proveedor</label>

                    <div class="col-md-6">
                        <select v-model="provider" id="provider" name="provider" :class="errors.provicer ? 'is-invalid' : ''" class="form-control" required autocomplete="provider" autofocus>
                            @foreach($providers as $provider)
                                <option value="{{$provider->id}}">{{$provider->name}}</option>
                            @endforeach
                        </select>
                        <div v-for="error in errors.provider" v-show="errors.provider" class="invalid-feedback">
                            @{{ error }}
                        </div>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" @click="closeModal">Cancelar</button>
                <button @click="save" type="button" class="btn btn-success">Guardar</button>
            </div>
        </div>
    </div>
</div>
