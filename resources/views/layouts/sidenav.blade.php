<link href="{{ asset('css/sidenav.css') }}" rel="stylesheet">

<div id="mySidebar" class="sidebar">
    <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>

    <div class="option">
        <a class="btn align-middle" href="/"> <i class="fa fa-home"></i>&nbsp;&nbsp;INICIO</a>
    </div>

    <div class="option">
        <a class="btn align-middle" href="/sell"> <i class="fa fa-cash-register"></i>&nbsp;&nbsp;VENDER</a>
    </div>

    <div class="option">
        <a class="btn align-middle" href="/users"> <i class="fa fa-users"></i>&nbsp;&nbsp;USUARIOS</a>
    </div>

    <div class="option">
        <a class="btn align-middle" href="/customers"> <i class="fa fa-users-cog"></i>&nbsp;&nbsp;CLIENTES</a>
    </div>

    <div class="option">
        <a class="btn align-middle" href="/products"> <i class="fa fa-apple-alt"></i>&nbsp;&nbsp;PRODUCTOS</a>
    </div>

    <div class="option">
        <a class="btn align-middle" href="/dishes"> <i class="fa fa-utensils"></i>&nbsp;&nbsp;PLATOS</a>
    </div>

    <div class="option">
        <a class="btn" href="/providers"><i class="fas fa-dolly"></i>&nbsp;&nbsp;PROVEEDORES</a>
    </div>

    <div class="option">
        <a class="btn" href="invoices"><i class="fas fa-file-invoice-dollar"></i>&nbsp;&nbsp;CUENTAS</a>
    </div>

</div>
