<nav class="navbar navbar-expand-md navbar-dark bg-dark shadow-sm">
    <div class="container-fluid">
        <button class="openbtn" onclick="openNav()"><img src="/images/icons/hamburger.svg" width="70px"> </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">

            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav ml-auto">
                <!-- Authentication Links -->
                @guest
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                    </li>
                    @if (Route::has('register'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                        </li>
                    @endif
                @else
                    <div id="user" class="container">
                        <div class="row">
                            <div class="col-md-8" style="margin-top: 5px">
                                <h2>{{ Auth::user()->person->first_name ." ". Auth::user()->person->last_name  }}</h2>
                                <p>{{ Auth::user()->userType->name}}</p>
                            </div>
                            <div class="col-md-4">
                                <img src="/images/icons/navuser.svg" width="70" alt="">
                            </div>
                        </div>

                    </div>
                    <li class="nav-item dropdown">
                        <a style="font-size: xx-large; margin-top: 10px; color: white;" id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            <span class="caret"></span>
                        </a>

                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                Cerrar Sesion
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </li>
                @endguest
            </ul>
        </div>
    </div>
</nav>
