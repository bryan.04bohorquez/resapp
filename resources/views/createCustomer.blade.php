<div class="modal fade" id="createCustomer" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">@{{ modalTitle }}</h5>
                <button type="button" class="close" @click="closeModal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <div class="form-group row">
                    <label for="first_name" class="col-md-4 col-form-label text-md-right">Nombre</label>

                    <div class="col-md-6">
                        <input v-model="first_name" id="first_name" type="text" class="form-control" name="first_name" required autocomplete="first_name" autofocus>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="first_name" class="col-md-4 col-form-label text-md-right">Apellido</label>

                    <div class="col-md-6">
                        <input v-model="last_name" id="last_name" type="text" class="form-control" name="last_name" required autocomplete="first_name" autofocus>
                    </div>
                </div>


                <div class="form-group row">
                    <label for="identification" class="col-md-4 col-form-label text-md-right">Identificación</label>
                    <div class="col-md-6">
                        <input v-model="identification" id="identification" type="text" class="form-control" name="identification" required autocomplete="identification" autofocus>
                    </div>
                </div>


                <div class="form-group row">
                    <label for="phone" class="col-md-4 col-form-label text-md-right">Telefono</label>

                    <div class="col-md-6">
                        <input v-model="phone" id="phone" type="text" class="form-control" name="phone" required autocomplete="phone" autofocus>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" @click="closeModal">Cancelar</button>
                <button @click="save" type="button" class="btn btn-success">Guardar</button>
            </div>
        </div>
    </div>
</div>
