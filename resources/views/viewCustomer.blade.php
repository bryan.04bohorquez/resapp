<div class="modal fade" id="detailCustomer" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" @click="closeDetail" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body" style="font-size: 18px">
                <p><strong>Nombre:</strong><br>@{{ customer.person.first_name + " " + customer.person.last_name }}</p>
                <p><strong>Identificación:</strong><br>@{{ customer.person.identification }}</p>
                <p><strong>Telefono:</strong><br>@{{ customer.person.phone }}</p>
                <p><strong>Creado:</strong><br>@{{ customer.created_at }}</p>
                <p><strong>Actualizado por ultima vez:</strong><br>@{{ customer.updated_at }}</p>
            </div>

        </div>
    </div>
</div>
