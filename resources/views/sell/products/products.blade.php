<div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
    <div class="row mb-4 mt-4">
        <label for="searchName" class="col-md-2 col-form-label text-md-right">Buscar:</label>
        <div class="col-md-10">
            <input @change="getProducts" v-model="searchName" class="form-control" name="searchName" required autocomplete="searchName" autofocus>
        </div>
    </div>

    <div class="row">
        <div v-if="isLoading" class="col-md-12" style="margin-top: 50px; margin-bottom: 50px;">
            <div class="row justify-content-center">
                <div class="col-md-12 text-center">
                    <div class="spinner-border" style="width: 3rem; height: 3rem; margin-bottom: 50px" role="status">
                        <span class="sr-only">Loading...</span>
                    </div>
                    <h3 style="text-align: center;">Cargando...</h3>
                </div>
            </div>
        </div>
        <div v-else v-for="product in products" class="col-md-4">
            <div style="margin-top: 10px" class="card">
                <div class="card-body">
                    <h5 class="card-title"><strong> @{{ product.name }}</strong></h5>
                    <p class="card-text">Precio Unidad:  @{{ product.price }}</p>
                    <div>
                        <hr>
                        <div class="form-group row">
                            <label for="quantity" class="col-md-6 col-form-label text-md-right">Cantidad</label>
                            <div class="col-md-6">
                                <input v-model="product.quantity" value="1" min="1" :max="product.stock" id="quantity" type="number" class="form-control" name="quantity" required autocomplete="quantity" autofocus>
                            </div>
                        </div>
                        <hr>
                        <h5 class="card-title" style="text-align: right">Total: @{{ product.total = product.quantity * product.price }}</h5>
                        <hr>
                        <a v-if="product.quantity >= 1" @click="addAccount(product)" class="btn btn-primary btn-block" style="background-color: black; color: white; border-color: black;">Añadir a cuenta</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <hr>

    <div class="row">
        <div class="col-md-12">
            <nav aria-label="Page navigation example">
                <ul class="pagination">
                    <li v-if="productsPagination.current_page > 1" class="page-item"><a @click.prevent="changePage(productsPagination.current_page - 1)" class="page-link" href="#">Anterior</a></li>

                    <li :class="[ page == isActived ? 'active' : '']" v-for="page in pagesNumber" class="page-item"><a @click.prevent="changePage(page)" class="page-link" href="#">@{{ page }}</a></li>

                    <li v-if="productsPagination.current_page < productsPagination.last_page" class="page-item"><a @click.prevent="changePage(productsPagination.current_page + 1)"  class="page-link" href="#">Siguiente</a></li>
                </ul>
            </nav>
        </div>
    </div>
</div>
