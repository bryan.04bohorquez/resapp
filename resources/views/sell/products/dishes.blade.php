<div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
    <div class="row mb-4 mt-4">
        <label for="searchName" class="col-md-2 col-form-label text-md-right">Buscar:</label>
        <div class="col-md-10">
            <input @change="getDishes" v-model="searchName" class="form-control" name="searchName" required autocomplete="searchName" autofocus>
        </div>
    </div>

    <div class="row">
        <div v-if="isLoadingD" class="col-md-12" style="margin-top: 50px; margin-bottom: 50px;">
            <div class="row justify-content-center">
                <div class="col-md-12 text-center">
                    <div class="spinner-border" style="width: 3rem; height: 3rem; margin-bottom: 50px" role="status">
                        <span class="sr-only">Loading...</span>
                    </div>
                    <h3 style="text-align: center;">Cargando...</h3>
                </div>
            </div>
        </div>
        <div v-else v-for="dish in dishes" class="col-md-4">
            <div style="margin-top: 10px" class="card">
                <div class="card-body">
                    <h5 class="card-title"><strong> @{{ dish.name }}</strong></h5>
                    <p class="card-text">Precio Unidad:  @{{ dish.price }}</p>
                    <div>
                        <hr>
                        <div class="form-group row">
                            <label for="quantity" class="col-md-6 col-form-label text-md-right">Cantidad</label>
                            <div class="col-md-6">
                                <input v-model="dish.quantity" value="1" min="1" type="number" class="form-control" name="quantity" required autocomplete="quantity" autofocus>
                            </div>
                        </div>
                        <hr>
                        <h5 class="card-title" style="text-align: right">Total: @{{ dish.total = dish.quantity * dish.price }}</h5>
                        <hr>
                        <a v-if="dish.quantity >= 1" @click="addAccount(dish)" class="btn btn-primary btn-block" style="background-color: black; color: white; border-color: black;">Añadir a cuenta</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <hr>

    <div class="row">
        <div class="col-md-12">
            <nav aria-label="Page navigation example">
                <ul class="pagination" v-if="dishes.length">
                    <li v-if="dishesPagination.current_page > 1" class="page-item"><a @click.prevent="changePageD(dishesPagination.current_page - 1)" class="page-link" href="#">Anterior</a></li>

                    <li :class="[ page == isActivedDishes ? 'active' : '']" v-for="page in pagesNumberDishes" class="page-item"><a @click.prevent="changePageD(page)" class="page-link" href="#">@{{ page }}</a></li>

                    <li v-if="dishesPagination.current_page < dishesPagination.last_page" class="page-item"><a @click.prevent="changePageD(dishesPagination.current_page + 1)"  class="page-link" href="#">Siguiente</a></li>
                </ul>
            </nav>
        </div>
    </div>
</div>
