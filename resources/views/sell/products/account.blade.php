<div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
    <br>

    <div class="form-group row">
        <label for="status" class="col-md-4 col-form-label text-md-right">Estado</label>

        <div class="col-md-6">
            <select v-model="status" id="status" name="status" class="form-control col" required autocomplete="status" autofocus>
                @foreach($status as $state)
                    <option value="{{$state->id}}">{{$state->name}}</option>
                @endforeach
            </select>
        </div>
    </div>

    <table class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Nombre</th>
            <th scope="col">Precio (Unidad)</th>
            <th scope="col">Cantidad</th>
            <th scope="col">Total</th>
            <th scope="col"></th>
        </tr>
        </thead>
        <tbody>
        <tr v-for="product in accountProducts">
            <td>@{{ product.id }}</td>
            <td>@{{ product.name }}</td>
            <td>@{{ product.price }}</td>
            <td>@{{ product.quantity }}</td>
            <td>@{{ product.total }}</td>
            <td>
                <a @click="deleteAccount(product)" class="btn btn-block" style="background-color: black; color: white;"> Quitar </a>
            </td>
        </tr>
        </tbody>
    </table>

    <hr>

    <h3 class="text-md-right">Total: $ @{{ paid_total }}</h3>

    <hr>

    <div class="row justify-content-center">
        <div :class="!invoiceId ? 'col-md-12' : 'col-md-7'">
            <button v-if="!edit" @click="createInvoice" type="button" class="btn btn-success btn-block">GUARDAR</button>
            <button v-else @click="updateInvoice()" type="button" class="btn btn-success btn-block">GUARDAR</button>
        </div>
        <div class="col-md-3">
            <a v-if="invoiceId" @click="generateInvoice(invoiceId)" class="btn btn-primary btn-block" style="color: white; background-color: black; border-color: black;"><i class="fas fa-file-pdf fa-lg"></i>&nbsp;Generar Factura</a>
        </div>
    </div>
</div>
