<div class="col-md-4">
    <div class="card">
        <div class="card-body">
            <h4>Cliente</h4>
            <hr>
            <div class="row">
                <label for="searchName" class="col-md-3 col-form-label text-md-left">Buscar:</label>
                <div class="col-md-9">
                    <input :disabled="edit" @change="getCustomer" v-model="searchCustomer" class="form-control" name="searchCustomer" required autocomplete="searchCustomer" autofocus>
                </div>
            </div>
            <hr>
            <div class="row">
                <label for="name" class="col-md-12 col-form-label text-md-left">Nombre</label>

                <div class="col-md-12">
                    <input :disabled="customer_id" v-model="first_name" id="name" type="text" class="form-control " name="name" autocomplete="name" autofocus>
                </div>

                <label for="name" class="col-md-12 col-form-label text-md-left">Apellido</label>

                <div class="col-md-12">
                    <input :disabled="customer_id" v-model="last_name" id="name" type="text" class="form-control " name="name" autocomplete="name" autofocus>
                </div>

                <label for="identification" class="col-md-12 col-form-label text-md-left">Identifición</label>

                <div class="col-md-12">
                    <input :disabled="customer_id" v-model="identification" id="identification" type="number" class="form-control" name="identification" required autocomplete="identification" autofocus>
                </div>

                <label for="phone" class="col-md-12 col-form-label text-md-left">Telefono</label>
                <div class="col-md-12">
                    <input :disabled="customer_id" v-model="phone" id="phone" type="text" class="form-control" name="phone" required autocomplete="phone" autofocus>
                </div>
            </div>
        </div>
    </div>
</div>
